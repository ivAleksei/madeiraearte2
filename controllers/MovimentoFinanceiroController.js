'use strict'
// MovimentoFinanceiroController.js

var md5 = require('md5');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var ObjectID = require('mongodb').ObjectID;

function MovimentoFinanceiroController(GenericModel) {
  this.model = GenericModel;
}

MovimentoFinanceiroController.prototype.getAll = function (req, res, next) {
  this.model.find(req.query, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

MovimentoFinanceiroController.prototype.getActive = function (req, res, next) {
  this.model.find({ ativo: true }, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};
MovimentoFinanceiroController.prototype.aggregate = function (req, res, next) {  
   console.log('aggregate')

  this.model.aggregate(req.query, function (err, data) {
    if (err) return next(err);
   console.log(data)
    
    res.json(data);
  });
};
MovimentoFinanceiroController.prototype.getById = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;
  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    }

    res.json(data);
  });
};
MovimentoFinanceiroController.prototype.getAnoMovimentos = function (req, res, next) {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  var _id = req.params._id;
  var model = this.model;
  model.find({}, function (err, data) {
    if (data.length) {
      res.json(data.map(function (r) {
        return new Date(r.pagamento.vencimento).getYear() + 1900;
      }).filter(onlyUnique).map(function (r) {
        return { ano: r }
      }))
    } else {
      var anos = { ano: new Date().getYear() + 1900 }
      res.json([anos])
    }

  })
};

MovimentoFinanceiroController.prototype.getBalanco = function (req, res, next) {
  var q = req.query;
  var queryMatch = { ativo: true }

  if (q._c) {
    queryMatch['pagamento._conta'] = new ObjectID(q._c);
  }
  if (q.h) {
    queryMatch['pagamento.vencimento'] = { $lte: new Date() }
  }

  if (q.ano || q.mes) {
    var inicio = new Date(q.ano + '-' + q.mes + '-' + 1);
    var fim = new Date(q.ano + '-' + q.mes + '-' + 31);
    queryMatch['pagamento.vencimento'] = { $gte: inicio, $lte: fim }
  }
  req.query = [
    { $match: queryMatch },
    {
      $group: {
        _id: { 'pago': '$pago' },
        'parcelado': { $sum: '$pagamento.valores.parcelado' }
      }
    }
  ];
  this.model.aggregate(req.query, function (err, data) {
    if (err) return next(err);

    var total = 0;
    var pago = 0;
    for (let i in data) {
      total += data[i].parcelado;

      if (data[i]._id.pago)
        pago = data[i].parcelado;
    }

    res.json({
      total: total || 0,
      pago: pago || 0
    });
  });

}
MovimentoFinanceiroController.prototype.create = function (req, res, next) {
  var model = this.model;
  var obj = req.body;

  var persist = function (obj) {
    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    for (let i = 0; i < req.query.n; i++) {
      let duration;
      let interval;
      switch (req.query.p) {
        case 1:
          duration = 1;
          interval = 'w';
          break;
        case 2:
          duration = 1;
          interval = 'M';
          break;
        case 3:
          duration = 2;
          interval = 'w';
          break;
      }
      obj.pago = false;
      obj.pagamento.numeroParcela = i + 1;
      obj.pagamento.dataReferencia = new Date(obj.pagamento.dataReferencia)

      // FUNÇÃO RECURSIVA - CUIDADO AO MEXER
      var populate = function (obj, c) {
        for (let f in obj)
          if (String(obj[f])) {
            if (typeof obj[f] == 'object') {
              populate(obj[f], (c ? c + '.' + f : f))
            }
          } else {
            delete obj[f];
          }
      }
      populate(obj, '')

      obj.pagamento.vencimento = moment(obj.pagamento.dataReferencia).add(i, 'M');

      if (obj.id && obj._id != '') {
        var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
        err.status = 400;
        return next(err);
      }
      model.create(obj, function (err, data) {
        if (err) {

          return next(err);
        }
        if (i == req.query.n - 1)
          res.json(data);
      });
    }
  }
  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj);
    });
  } else {
    persist(obj);
  }
};
MovimentoFinanceiroController.prototype.update = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;
  var obj = req.body;
  var persist = function (obj) {
    if (!obj._id || obj._id == '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    // FUNÇÃO RECURSIVA - CUIDADO AO MEXER
    var populate = function (obj, c) {
      for (let f in obj)
        if (String(obj[f])) {
          if (typeof obj[f] == 'object') {
            populate(obj[f], (c ? c + '.' + f : f))
          }
        } else {
          delete obj[f];
        }
    }
    populate(obj, '')

    model.update(_id, obj, function (err, data) {
      if (err) return next(err);
      model.findOne(_id, function (err, data) {
        if (err) return next(err);
        res.json(data[0]);
      });
    });
  }
  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj);
    });
  } else {
    persist(obj);
  }
};
MovimentoFinanceiroController.prototype.remove = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    } else {
      var _id = data[0]._id;
      model.remove(_id, function (err, data) {
        if (err) return next(err);
        res.json('ok');
      });
    }
  });
}
MovimentoFinanceiroController.prototype.removeArray = function (req, res, next) {

  var model = this.model;

  var obj = req.body;
  var remove = function (obj) {
    if (!obj.length) {
      var err = new Error('Nenhum parâmetro para exclusão de objeto');
      err.status = 400;
      return next(err);
    }
    for (let i in obj) {
      model.findOne(obj[i], function (err, data) {
        if (err) return next(err);
        if (!data) {
          var err = new Error('Not Found');
          err.status = 404;
          return next(err);
        } else {
          var _id = data[0]._id;

          model.remove(_id, function (err, data) {
            if (err) return next(err);
            if (i == obj.length - 1)
              res.json('ok')
          });
        }
      });
    }
  }
  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      remove(obj);
    });
  } else {
    remove(obj);
  }

}

module.exports = function (GenericModel) {
  return new MovimentoFinanceiroController(GenericModel);
};