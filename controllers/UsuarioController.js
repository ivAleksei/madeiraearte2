'use strict'

// GenericController.js

var md5 = require('md5');
var fs = require('fs');
var path = require('path');
var userPicURL = 'files/images/user_pics/';
var userPicFolder = path.join(__dirname, '..');


for (let i in userPicURL.split('/')) {
  userPicFolder = path.join(userPicFolder, userPicURL.split('/')[i]);
  if (userPicURL.split('/')[i])
    fs.mkdir(userPicFolder, function (err, data) {
      if (err) { }
    })
}

function orderAlfabetica(a, b) {
  return (a.nome > b.nome);
}

function GenericController(GenericModel) {
  this.model = GenericModel;
}

GenericController.prototype.getAll = function (req, res, next) {
  this.model.find({}, function (err, data) {
    if (err) return next(err);
    res.json(data.map(function (u) {
      return {
        _id: u._id,
        nome: u.nome,
        login: u.login_exib,
        user_pic: u.user_pic.ext ? u._id + u.user_pic.ext : '',
        _modulos: u._modulos.map(function (m) {
          return m._id;
        }),
        _paginas: u._paginas.map(function (p) {
          return p._id;
        }),
        ativo: u.ativo
      }
    }));
  });
};

GenericController.prototype.getActive = function (req, res, next) {
  this.model.find({ ativo: 1 }, function (err, data) {
    if (err) return next(err);
    res.json(data.map(function (u) {
      return {
        _id: u._id,
        nome: u.nome,
        login: u.login_exib,
        user_pic: u.user_pic.ext ? u._id + u.user_pic.ext : '',
        _modulos: u._modulos.map(function (m) {
          return m._id;
        }),
        _paginas: u._paginas.map(function (p) {
          return p._id;
        }),
        ativo: u.ativo
      }
    }));
  });
};

GenericController.prototype.getUserInfo = function (req, res, next) {
  var token = req.query.token;
  var _id = req.params._id;
  var UsuarioModel = this.model;

  UsuarioModel.find({ ativo: 1, _id: _id }, function (err, data) {
    if (err) return next(err);

    res.json(data.map(function (u) {
      return {
        _id: u._id,
        nome: u.nome,
        login: u.login_exib,
        user_pic: u.user_pic.ext ? u._id + u.user_pic.ext : '',
        modulosAutorizados: u._modulos
          .filter(function (m) {
            return m.ativo;
          }).map(function (m) {
            return {
              _id: m._id,
              nome: m.nome,
              codigo: m.codigo,
              icone: m.icone,
              paginas: u._paginas.filter(function (p) {
                if (p._modulo.toString() == m._id.toString())
                  return p && p.ativo
              }).map(function (p) {
                return {
                  _id: p._id,
                  nome: p.nome,
                  codigo: p.codigo
                }
              }).sort(orderAlfabetica)
            }
          }).sort(orderAlfabetica),
        ativo: u.ativo,
        token: token
      }
    })[0]);
  });
};


GenericController.prototype.getById = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    }

    res.json(data);
  });

};

GenericController.prototype.create = function (req, res, next) {

  var model = this.model;
  var obj = req.body;

  var persist = function (obj, files) {
    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    /* ENCRIPTAÇÃO DADOS DE LOGIN */
    obj.login = md5(obj.login)
    obj.password = md5(obj.login + obj.password)

    obj.ativo = true;
    model.create(obj, function (err, data) {
      if (err) return next(err);

      if (files)
        for (let f in files) {

          var ext = files[f].name.substring(files[f].name.lastIndexOf('.'));
          fs.renameSync(files[f].path, path.join(userPicFolder, data._id + ext));
        }
      var dataResp = [data];

      res.json(dataResp.map(function (u) {
        return {
          _id: u._id,
          nome: u.nome,
          login: u.login_exib,
          user_pic: u.user_pic.ext ? u._id + u.user_pic.ext : '',
          _modulos: u._modulos.map(function (m) {
            return m._id;
          }),
          _paginas: u._paginas.map(function (p) {
            return p._id;
          }),
          ativo: u.ativo
        }
      })[0]);
    });
  }



  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

GenericController.prototype.removePic = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  var obj = { "_id": _id, "user_pic": {} };

  model.update(_id, obj, function (err, data) {
    if (err) return next(err);
    res.json('ok');
  });
}

GenericController.prototype.update = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;
  var obj = req.body;

  var persist = function (obj, files) {
    if (Object.keys(files).length > 0) {
      var filesFolder = fs.readdirSync(userPicFolder)

      filesFolder.forEach(file => {
        if (file.includes(_id)) {
          fs.unlinkSync(path.join(userPicFolder, file));
        }
      });

      for (let f in files) {
        var ext = files[f].name.substring(files[f].name.lastIndexOf('.'));
        fs.renameSync(files[f].path, path.join(userPicFolder, _id + ext));
      }
    }

    /* RENOVAÇÃO DE PASSWORD */
    if (obj.login) {
      obj.login = md5(obj.login_exib)
      obj.password = md5(obj.login + obj.password)
    }

    model.update(_id, obj, function (err, data) {
      if (err) return next(err);
      model.findOne(_id, function (err, data) {
        if (err) return next(err);

        res.json(data.map(function (u) {
          return {
            _id: u._id,
            nome: u.nome,
            login: u.login_exib,
            user_pic: u.user_pic.ext ? u._id + u.user_pic.ext : '',
            _modulos: u._modulos.map(function (m) {
              return m._id;
            }),
            _paginas: u._paginas.map(function (p) {
              return p._id;
            }),
            ativo: u.ativo
          }
        })[0]);
      });
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

GenericController.prototype.remove = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    } else {
      var _id = data[0]._id;

      var filesFolder = fs.readdirSync(userPicFolder);

      filesFolder.forEach(file => {
        if (file.includes(_id)) {
          fs.unlinkSync(path.join(userPicFolder, file));
        }
      });


      model.remove(_id, function (err, data) {
        if (err) return next(err);
        res.json('ok');
      });
    }
  });
}

module.exports = function (GenericModel) {
  return new GenericController(GenericModel);
};