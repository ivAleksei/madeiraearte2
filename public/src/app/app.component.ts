import { Component, NgModule, Inject, LOCALE_ID } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor( @Inject(LOCALE_ID) localeId) { }
  
  title = 'app works!';
  environmentName = environment.envName;

  notifications_opt: any = {
    position: ["top", "right"],
    preventDuplicates: true
  }
}
