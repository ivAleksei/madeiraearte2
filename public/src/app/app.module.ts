import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { SweetAlertService } from 'ng2-sweetalert2';

import { Routing } from './app.routing';

import { AppComponent } from './app.component';
import { ApiService } from './shared/services/api.service';
import { MessagesAlertsService } from './shared/services/messages-alerts.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SimpleNotificationsModule.forRoot(),
    Routing
  ],
  providers: [
    ApiService,
    MessagesAlertsService,
    SweetAlertService,
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR'
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
