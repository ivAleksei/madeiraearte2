import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


const ROUTES: Routes = [
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

export const Routing = RouterModule.forRoot(ROUTES);  