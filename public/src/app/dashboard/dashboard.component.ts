import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../shared/services/api.service'

declare var $: any;
declare var window: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  modulos: any = [];
  userData: any = {};
  userId: any = '';
  info: any = {};
  color: string = ''
  mode: string = ''
  value: number = 0
  bufferValue: number = 0

  constructor(
    private ApiService: ApiService,
    private router: Router
  ) { }

  sidebarToggle: boolean;
  pathRouteComponents: string;
  statusTopMenu: boolean;

  ngOnInit(): void {
    this.statusTopMenu = false;
    
    this.ApiService.checkAuth()
    this.getInfoClient();
    this.loadUserData();
    this.loadSideBar();
    this.configLoader();
  }

  getInfoClient(): void {
    this.ApiService.getInfoApp('client')
      .then(info => {
        this.info = info;
      })
      .catch(info => {
        this.info = {
          name: '',
          version: '',
          author: '',
          description: ''
        };
      })
  }

  loadUserData() {
    this.userId = window.sessionStorage.getItem('user_id');
    var router = this.router;

    if (this.userId) {
      this.ApiService.get('usuarios/_info/' + this.userId)
        .then((data: any) => {
          if (data) {
            this.userData = {
              _id: data._id,
              ativo: data.ativo,
              login: data.login,
              modulosAutorizados: data.modulosAutorizados,
              nome: data.nome,
              user_pic: data.user_pic
            }
            if (data.user_pic && !data.user_pic.includes('.'))
              delete this.userData.user_pic;
          }
        })
    }
    window.sessionStorage.removeItem('res');
  }

  loadSideBar() {
    this.sidebarToggle = false;
    this.ApiService.get('modulos')
      .then((data: any) => {
        if (data) {
          this.modulos = data;

          setTimeout(function () {
            $('.menu-pages').slideDown();
            $('body').addClass('sw-toggled');
            $('#l-load').fadeOut();
          }, 1000)

        }
      })
  }

  configLoader() {
    this.color = 'primary'
    this.mode = 'indeterminate'
    this.value= 40
    this.bufferValue = 0
  }

  ngAfterViewInit() {
    $('body').removeClass('login-content')

    $('#l-load').fadeOut()

    $('#content').click(() => {
      var target = $('#sidebar')
      if (target.hasClass('toggled'))
        target.removeClass('toggled')
    })
  }

}
