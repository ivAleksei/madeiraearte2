import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { DashboardRouting } from './dashboard.routing'

import { SharedModule } from "../shared/shared.module";

import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  imports: [
    CommonModule,
    DashboardRouting,
    SharedModule,  
    FormsModule
  ],
  declarations: [
    HeaderComponent,
    NavigationComponent,
    DashboardComponent,
    FooterComponent]
})
export class DashboardModule { }