import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const DASHBOARD_ROUTES: Routes = [
    {
        path: '', component: DashboardComponent, children: [
            { path: '', loadChildren: '../pages/home/home.module#HomeModule' },
            { path: 'sys-admin', loadChildren: '../pages/sys-admin/sys-admin.module#SysAdminModule' },
            { path: 'financeiro', loadChildren: '../pages/financeiro/financeiro.module#FinanceiroModule' },
            { path: 'cadsimples', loadChildren: '../pages/cadsimples/cadsimples.module#CadSimplesModule' },
            { path: 'prod-servicos', loadChildren: '../pages/prod-servicos/prod-servicos.module#ProdServicosModule' }
        ]
    }
];

export const DashboardRouting = RouterModule.forChild(DASHBOARD_ROUTES);
