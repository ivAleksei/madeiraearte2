import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
declare var window: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input()
  userData: any = {}

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  privacySettings(): void {
    alert('You are forbidden!');
  }
  otherSettings(): void {
    alert('Some Settings!');
  }

  toggleFullscreen(): void {
    alert('Go to a eye doctor!');
  }

  clearStorage(): void {
    window.localStorage.clear();
    window.sessionStorage.clear();
  }
  logout(): void {
    var router = this.router;
    this.clearStorage();

    $('#l-load').fadeIn(function () {
      $('body').addClass('login-content');
      router.navigateByUrl('/login');
      $('#l-load').fadeOut();
    });
  }
}
