import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArquitetosComponent } from './arquitetos.component';

describe('ArquitetosComponent', () => {
  let component: ArquitetosComponent;
  let fixture: ComponentFixture<ArquitetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArquitetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArquitetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
