import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

declare var $: any;
declare var moment: any

@Component({
  selector: 'app-arquiteto',
  templateUrl: './arquitetos.component.html',
  styleUrls: ['./arquitetos.component.scss']
})
export class ArquitetosComponent implements OnInit {

  arquitetos: any = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {
    this.loadArquitetos();
  }

  ngOnInit() {

  }

  loadArquitetos(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('arquitetos')
      .then((data: any) => {
        this.arquitetos = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }

  add(form_directive: any, arquitetos: any): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('arquitetos', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.arquitetos.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, arquitetos: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('arquitetos', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        console.log(arquitetos);

        this.arquitetos = arquitetos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, arquitetos: any): void {

    let obj = form_directive.value

    this.ApiService.delete('arquitetos', obj).then(data => {
      if (data) {
        this.arquitetos = arquitetos.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true
    object.edit = this.edit;
    object = this.parseForm(object)

    delete object.$$index
    form_directive.form.setValue(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset();
    $(':input').val('');
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    if ($('input[name="dataNascimento"]').val() != '')
      form_directive.value.dataNascimento = moment($('input[id="dataNascimento"]').val(), "DD/MM/YYYY").utc().format('YYYY-MM-DD')
    return form_directive.value
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    if (!parse.edit) {
      parse.dataNascimento = obj.dataNascimento ? moment(obj.dataNascimento).utc().format('DD/MM/YYYY') : null;
    }
    delete parse.$$index
    delete parse.edit
    return parse
  }

}
