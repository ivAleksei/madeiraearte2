import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";
import { ClientesComponent } from "./clientes/clientes.component";
import { FornecedoresComponent } from './fornecedores/fornecedores.component';
import { ArquitetosComponent } from './arquitetos/arquitetos.component';
import { AmbientesComponent } from './ambientes/ambientes.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { MateriaisComponent } from './materiais/materiais.component';


const CADSIMPLES_ROUTE = [
  { path: 'clientes', component: ClientesComponent },
  { path: 'fornecedores', component: FornecedoresComponent },
  { path: 'arquitetos', component: ArquitetosComponent },
  { path: 'ambientes', component: AmbientesComponent },
  { path: 'funcionarios', component: FuncionariosComponent },
  { path: 'materiais', component: MateriaisComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(CADSIMPLES_ROUTE)
  ],
  declarations: [
    ClientesComponent,
    FornecedoresComponent,
    ArquitetosComponent,
    AmbientesComponent,
    FuncionariosComponent,
    MateriaisComponent
  ],
  exports: [
    ClientesComponent,
    FornecedoresComponent,
    ArquitetosComponent,
    AmbientesComponent,
    FuncionariosComponent,
    MateriaisComponent
  ]
})
export class CadSimplesModule { }
