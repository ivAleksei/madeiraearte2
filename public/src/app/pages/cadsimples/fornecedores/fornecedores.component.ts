import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

declare var $: any;
declare var moment: any

@Component({
  selector: 'app-fornecedor',
  templateUrl: './fornecedores.component.html',
  styleUrls: ['./fornecedores.component.scss']
})
export class FornecedoresComponent implements OnInit {

  fornecedores: any = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {
    this.loadFornecedores();
  }

  ngOnInit() {

  }

  loadFornecedores(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('fornecedores')
      .then((data: any) => {
        this.fornecedores = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }

  add(form_directive: any, fornecedores: any): void {
    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('fornecedores', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.fornecedores.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, fornecedores: any): void {
    let obj = this.stringForm(form_directive)
    this.ApiService.put('fornecedores', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)


        this.fornecedores = fornecedores.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, fornecedores: any): void {

    let obj = form_directive.value

    this.ApiService.delete('fornecedores', obj).then(data => {
      if (data) {
        this.fornecedores = fornecedores.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true
    object.edit = this.edit;
    object = this.parseForm(object)

    delete object.$$index
    form_directive.form.setValue(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $(':input').val('');
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    if ($('input[name="dataFundacao"]').val() != '')
      form_directive.value.dataFundacao = moment($('input[id="dataFundacao"]').val(), "DD/MM/YYYY").utc().format('YYYY-MM-DD')
    if ($('input[name="data_nascimento"]').val() != '')
      form_directive.value.contato.principal.data_nascimento = moment($('input[id="data_nascimento"]').val(), "DD/MM/YYYY").utc().format('YYYY-MM-DD')
    return form_directive.value
  }
  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    if (!parse.edit) {
      parse.dataFundacao = parse.dataFundacao ? moment(parse.dataFundacao).utc().format('DD/MM/YYYY') : null;
      parse.contato.principal.data_nascimento = parse.contato.principal.data_nascimento ? moment(parse.contato.principal.data_nascimento).utc().format('DD/MM/YYYY') : null;
    }
    return parse
  }

}
