import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service'

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-funcionarios',
  templateUrl: './funcionarios.component.html',
  styleUrls: ['./funcionarios.component.scss']
})
export class FuncionariosComponent implements OnInit {

  edit: Boolean = false;
  funcionarios: any[] = [];

  constructor(
    private ApiService: ApiService
  ) {
    this.loadFuncionarios();
  }

  ngOnInit() {
  }

  loadFuncionarios(): void {

    this.ApiService.get('funcionarios/all')
      .then((data) => {

        this.funcionarios = data.map((mod: any) => {
          return this.parseForm(mod)
        })
      })
  }

  add(form_directive: any, funcionarios: any): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('funcionarios', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.funcionarios.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, funcionarios: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('funcionarios', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        this.funcionarios = funcionarios.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, funcionarios: any): void {

    let obj = form_directive.value

    this.ApiService.delete('funcionarios', obj).then(data => {
      if (data) {
        this.funcionarios = funcionarios.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset();
    $(':input').val('');
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    if ($('input[name="data_nascimento"]').val() != '')
      form_directive.value.data_nascimento = moment($('input[id="data_nascimento"]').val(), "DD/MM/YYYY").utc().format('YYYY-MM-DD')
    return form_directive.value
  }

  
  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    if (!parse.edit) {
      parse.data_nascimento = parse.data_nascimento ? moment(parse.data_nascimento).utc().format('DD/MM/YYYY') : null;
    }
    return parse
  }

  select(object: any, form_directive: any): void {
    this.edit = true
    object.edit = this.edit;
    object = this.parseForm(object)

    delete object.$$index
    form_directive.form.setValue(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }
}
