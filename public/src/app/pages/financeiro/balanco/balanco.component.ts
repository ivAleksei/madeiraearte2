import { Component, OnInit, ViewChild } from '@angular/core'
import { ApiService } from '../../../shared/services/api.service'
import { environment } from '../../../../environments/environment';

declare var $: any
declare var moment: any

@Component({
  selector: 'app-balanco',
  templateUrl: './balanco.component.html',
  styleUrls: ['./balanco.component.scss']
})
export class BalancoComponent implements OnInit {

  /* COLLECTIONS */
  clientes: any = []
  planocontas: any = []
  contas: any = []
  formapagamentos: any = []
  materiais: any = []
  balanco: any = []
  anosMovimento: any = []
  receitas: any = []
  despesas: any = []
  saldoContas: any[] = []

  /* OBJETOS */
  parcelamento: { tipo: Boolean, numero: Number } = { tipo: false, numero: 0 }
  filtro: any = {
    mes: new Date().getMonth() + 1,
    ano: new Date().getFullYear()
  }

  /* VARIAVEIS */
  edit: Boolean = false
  tab: Number = 0
  contaSaldo : string = ''



  constructor(
    private ApiService: ApiService,
  ) {
    this.loadMovimentos()
    this.loadAnoMovimento()
    this.loadBalanco()
    this.loadSelects()
    this.loadSaldoContas()
  }

  ngOnInit() {
    this.ApiService.checkAuth()
  }

  loadSelects(): void {
    let selects = ['clientes', 'planocontas', 'contas', 'formapagamentos']
    for (let i in selects) {
      this.ApiService.get(selects[i])
        .then((data: any) => {
          this[selects[i]] = data.map((c: any) => {
            return this.parseForm(c)
          })
        })
    }
  }

  loadAnoMovimento(): void {
    function onlyUnique(v: any, i: any, s: any) {
      return s.indexOf(v) === i
    }
    let parseForm = this.parseForm
    this.ApiService.get('receitas/arc')
      .then((data: any) => {
        this.anosMovimento = data
        this.ApiService.get('despesas/arc')
          .then((data: any) => {
            this.anosMovimento = this.anosMovimento.concat(data).filter(onlyUnique)
          })
      })
  }

  loadBalanco(): void {
    let parseForm = this.parseForm
    this.ApiService.get('receitas/balanco?ano=' + this.filtro.ano + '&mes=' + this.filtro.mes)
      .then((data: any) => {
        this.balanco.receita = data
        $('#receitas-progress-bar').css({ 'width': data.total != 0 ? (data.pago / data.total) * 100 + '%' : '0%' })
      })
    this.ApiService.get('despesas/balanco?ano=' + this.filtro.ano + '&mes=' + this.filtro.mes)
      .then((data: any) => {
        this.balanco.despesa = data
        $('#despesas-progress-bar').css({ 'width': data.total != 0 ? (data.pago / data.total) * 100 + '%' : '0%' })
      })
  }
  loadSaldoContas() {
  this.contaSaldo = ''
    this.saldoContas = []
    this.ApiService.get('contas/saldo')
      .then((data: any) => {
        this.saldoContas = data
      })
  }

  loadMovimentos(): void {
    var parseForm = this.parseForm

    this.receitas = []
    this.despesas = []

    this.ApiService.get('receitas/all?ano=' + this.filtro.ano + '&mes=' + this.filtro.mes)
      .then((data: any) => {
        $('#table-filtered-receitas').DataTable().destroy()
        this.receitas = data.map(function (c: any) {
          return parseForm(c)
        })
      })
    this.ApiService.get('despesas/all?ano=' + this.filtro.ano + '&mes=' + this.filtro.mes)
      .then((data: any) => {
        $('#table-filtered-despesas').DataTable().destroy()
        this.despesas = data.map(function (c: any) {
          return parseForm(c)
        })
      })
      this.loadSaldoContas()
  }

  add(form_directive: any): void {
    let movimento = this.tab == 0 ? 'receitas' : 'despesas'
    let route = movimento + '?n=' + this.parcelamento.numero + '&p=' + this.parcelamento.tipo
    let obj = this.stringForm(form_directive)

    obj.ativo = true

    if (obj)
      this.ApiService.post(route, obj, null)
        .then(data => {
          if (data) {
            console.log(data)
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)
            obj._cliente = this.clientes.filter((c)=>{
              return c._id == data._cliente
            })[0]

            obj.pagamento._formapagamentos = this.formapagamentos.filter((f)=>{
              return f._id == data.pagamento._formapagamentos
            })[0]
            

            if (obj._id) {
              this[movimento].push(obj)
              this.clear(form_directive)
              $('#btn-close-modal').click()
            }
          }
        })
  }

  update(form_directive: any): void {
    let movimento = this.tab == 0 ? 'receitas' : 'despesas'
    let objetos = this[movimento]
    let obj = this.stringForm(form_directive)

    this.ApiService.put(movimento, obj, null).then(data => {
      if (data) {

        let parse = this.parseForm(data)
        this[movimento] = objetos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
        $('#btn-close-modal').click()
      }
    })
  }

  removerMovimentos() {
    let array: any[];
    let schema = '';

    let movimento = this.tab == 0 ? 'receitas' : 'despesas'

    array = this[movimento].filter((m: any) => { return m.check }).map((m: any) => { return m._id });

    this.ApiService.deleteArray(movimento, array).then(data => {
      if (data) {
        this.selectInterval(this.filtro.mes, this.filtro.ano);
      }
    });
  }

  remove(form_directive: any, balanco: any): void {

    let route = this.tab == 0 ? 'receitas' : 'despesas'
    let obj = form_directive.value

    this.ApiService.delete(route, obj).then(data => {
      if (data) {
        this.balanco = balanco.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(movimento: any, form_directive: any): void {
    this.edit = true

    let object = JSON.parse(JSON.stringify(movimento))

    object = this.parseForm(object)

    delete object.$$index

    if (object._cliente && object._cliente._id)
      object._cliente = object._cliente._id
    if (object.pagamento && object.pagamento._planocontas && object.pagamento._planocontas._id)
      object.pagamento._planocontas = object.pagamento._planocontas._id
    if (object.pagamento && object.pagamento._conta && object.pagamento._conta._id)
      object.pagamento._conta = object.pagamento._conta._id
    if (object.pagamento && object.pagamento._formapagamentos && object.pagamento._formapagamentos._id)
      object.pagamento._formapagamentos = object.pagamento._formapagamentos._id


    form_directive.form.setValue(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh')
  }

  pagar(form_directive: any, status: Boolean): void {
    let obj = {
      _id: form_directive.value._id,
      pago: status
    }
    if (this.tab == 0) {
      var schema = 'receitas'
    } else {
      var schema = 'despesas'
    }

    this.ApiService.put(schema, obj, null).then(data => {
      if (data) {
        this.edit = false
        this.loadMovimentos()
        this.loadBalanco()
        this.clear(form_directive)

        $('#btn-close-modal').click()
      }
    })
  }

  emitirRecibo(receitas: any[]) {
    let notFound = true
    for (let i in receitas) {
      if (receitas[i].check) {
        notFound = false
        window.open(environment.urlAPI + '/ws/reports?rpt=recibo&sch=receitas&_id=' + receitas[i]._id, '_blank')
      }
    }
  }

  selectInterval(mes: any, ano: any) {
    this.filtro = {
      mes: mes,
      ano: ano
    }
    this.loadBalanco()
    this.loadMovimentos()
  }

  _formaPagamentoChange($event: any, form_directive: any) {
    this.parcelamento = {
      tipo: false,
      numero: 0
    }

    this.formapagamentos.filter((f): void => {
      if (f._id == form_directive.value.pagamento._formapagamentos)
        this.parcelamento = f.parcelamento
    })
  }

  calculaParcela(form_directive: any) {
    let form = form_directive.value
    let total = form.pagamento.valores.total.valueOf()
    let qnt = this.parcelamento.numero.valueOf()
    if (total && qnt) {
      form = {
        "_id": form._id || '',
        "pago": form.pago || '',
        "ativo": form.ativo || '',
        "pagamento": {
          "valores": {
            "total": form.pagamento.valores.total || '',
            "parcelado": (total / qnt).toFixed(2)
          },
          "dataReferencia": form.pagamento.dataReferencia,
          "vencimento": form.pagamento.vencimento,
          "numeroParcela": form.pagamento.numeroParcela || '',
          "_formapagamentos": form.pagamento._formapagamentos || '',
          "_planocontas": form.pagamento._planocontas || '',
          "_conta": form.pagamento._conta || ''
        },
        "observacoes": form.observacoes || '',
        "_cliente": form._cliente || ''
      }

      form = this.parseForm(form)
      form_directive.form.setValue(form)
    }
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
    $('.selectpicker').selectpicker('refresh')
  }

  stringForm(form_directive: any): any {
    console.log(form_directive)

    this.ApiService.populate(form_directive.value, '')

    if ($('input[name="vencimento"]').val() != '')
      form_directive.value.pagamento.vencimento = moment($('input[name="vencimento"]').val(), "DD/MM/YYYY").toISOString()
    form_directive.value.pagamento.dataReferencia = moment($('input[name="dataReferencia"]').val(), 'DD/MM/YYYY').toISOString();

    return form_directive.value
  }

  
  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }

}
