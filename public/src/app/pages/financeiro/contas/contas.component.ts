import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

declare var $: any;

@Component({
  selector: 'app-conta',
  templateUrl: './contas.component.html',
  styleUrls: ['./contas.component.scss']
})
export class ContasComponent implements OnInit {

  contas: any = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {
    this.loadContas();
  }

  ngOnInit() {

  }

  loadContas(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('contas')
      .then((data: any) => {
        this.contas = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }

  add(form_directive: any, contas: any): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('contas', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.contas.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, contas: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('contas', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        console.log(contas);

        this.contas = contas.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, contas: any): void {

    let obj = form_directive.value

    this.ApiService.delete('contas', obj).then(data => {
      if (data) {
        this.contas = contas.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true
    object.edit = this.edit;
    object = this.parseForm(object)

    delete object.$$index
    form_directive.form.setValue(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset();
    $(':input').val('');
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }


  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }
}
