import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";

import { BalancoComponent} from "./balanco/balanco.component";
import { ContasComponent } from './contas/contas.component';
import { FormasPagamentoComponent } from './formas-pagamento/formas-pagamento.component';
import { PlanoContasComponent } from './plano-contas/plano-contas.component';


const FINANCEIRO_ROUTES = [
  { path: 'balanco', component: BalancoComponent },
  { path: 'contas', component: ContasComponent },
  { path: 'formaPagamentos', component: FormasPagamentoComponent },
  { path: 'planocontas', component: PlanoContasComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(FINANCEIRO_ROUTES)
  ],
  declarations: [
    BalancoComponent,
    ContasComponent,
    FormasPagamentoComponent,
    PlanoContasComponent
  ],
  exports: [
    BalancoComponent,
    ContasComponent,
    FormasPagamentoComponent,
    PlanoContasComponent
  ] 
})
export class FinanceiroModule { }
