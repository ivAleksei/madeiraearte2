import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

declare var $:any;

@Component({
  selector: 'app-plano-contas',
  templateUrl: './plano-contas.component.html',
  styleUrls: ['./plano-contas.component.scss']
})
export class PlanoContasComponent implements OnInit {

  planocontas: any = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {     
    this.loadPlanoContas();
  }

  ngOnInit() {
    
  }

  loadPlanoContas(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('planocontas')
      .then((data: any) => {
        this.planocontas = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }

  add(form_directive: any, planocontas: any): void {

    var obj = this.stringForm(form_directive)
    
    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('planocontas', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.planocontas.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, planocontas: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('planocontas', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        
        this.planocontas = planocontas.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, planocontas: any): void {

    let obj = form_directive.value

    this.ApiService.delete('planocontas', obj).then(data => {
      if (data) {
        this.planocontas = planocontas.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true
   
    object = this.parseForm(object)

    form_directive.form.setValue(object)
    
    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }

  
  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }

}