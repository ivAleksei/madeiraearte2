import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service'

declare var $: any

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  eventos: any[] = null

  constructor(
    private ApiService: ApiService
  ) {
    this.loadEvents()
  }

  ngOnInit() {
  }

  loadEvents(): void {
    this.ApiService.get('agenda')
      .then((data) => {
        if (data)
          this.eventos = data.map((d: any) => {
            return this.parseForm(d)
          })
      })
  }

  
  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    parse.id = parse._id
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }
}
