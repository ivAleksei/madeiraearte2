import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";

const HOME_ROUTES = [
    { path: '', component: HomeComponent }
];

@NgModule({
  imports: [    
    CommonModule,
    FormsModule,
    SharedModule,    
    RouterModule.forChild(HOME_ROUTES)
  ],
  declarations: [
    HomeComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
