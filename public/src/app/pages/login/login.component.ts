import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../shared/services/api.service';
import { Md5 } from 'ts-md5/dist/md5';

declare var $: any;
declare var window: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private ApiService: ApiService
  ) { }

  /* DECLARE VARIABLES */
  info: any = {
    name: '',
    version: '',
    author: '',
    description: ''
  };

  chkManterSessao: boolean = false;

  block: number = 0;

  ngOnInit() {
    this.getInfoClient();
    //this.logar(null, true);
  }

  ngAfterViewInit() {
    $('body').addClass('login-content');
    $('body').removeClass('sw-toggled');
    $('#l-load').fadeOut();
  }

  getInfoClient(): void {
    this.ApiService.getInfoApp('client')
      .then(info => {
        this.info = info;
      })
  }

  logar(form_directive: any, bypass: boolean): void {

    let user: any;

    if (!bypass) {
      user = {
        login: form_directive.value.login,
        password: form_directive.value.senha
      }
    } else {
      user = {
        login: 'admin',
        password: '1234'
      }
    }

    user.login = Md5.hashStr(user.login)
    user.password = Md5.hashStr(user.login + user.password)

    this.ApiService.post('usuarios/login', user, null)
      .then((res: any) => {
        if (res.token) {
          if (this.chkManterSessao) {
            window.localStorage.setItem('x-access-token', res.token);
          } else {
            window.sessionStorage.setItem('x-access-token', res.token);
          }

          window.sessionStorage.setItem('user_id', res._id);
          window.sessionStorage.setItem('res', JSON.stringify(res));

          var router = this.router;
          $('#l-load').fadeIn(function () {
            $('body').addClass('login-content');
            router.navigateByUrl("/dashboard");
          });
        }
      })
  }

  registrar(form_directive: any, bypass: boolean): void {
    let user: any = {
        login: form_directive.value.login,
        password: form_directive.value.senha
      }

    user.login = Md5.hashStr(user.login)
    user.password = Md5.hashStr(user.login + user.password)
    this.ApiService.post('usuarios', user, null)
      .then((res: any) => {
        
      })
  }

  setBlock(value:number){
    this.block = value;
  }

}
