import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";

const LOGIN_ROUTES = [
    { path: '', component: LoginComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,    
    RouterModule.forChild(LOGIN_ROUTES)
  ],
  providers: [],
  declarations: [LoginComponent]
})
export class LoginModule { }
