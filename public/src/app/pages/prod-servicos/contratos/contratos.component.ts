import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { environment } from '../../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-contrato',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.scss']
})
export class ContratosComponent implements OnInit {
  contratos: any[] = [];
  orcamentos: any[] = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService
  ) {
    this.loadContratos();
    this.loadOrcamentos();
  }

  ngOnInit() {
  }

  loadContratos(): void {
    this.ApiService.get('contratos')
      .then((data: any) => {
        console.log(data);
        this.contratos = data.map((c: any) => {
          return this.parseForm(c);
        });
      })
  }

  loadOrcamentos(): void {
    this.ApiService.get('orcamentos/aprovados')
      .then((data: any) => {
        this.orcamentos = data;
      });
  }

  add(form_directive: any, contratos: any): void {
    var obj = this.stringForm(form_directive)
    form_directive.value.ativo = true;
    if (obj)
      this.ApiService.post('contratos', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))
            obj = this.parseForm(obj)
            if (obj._id) {
              this.loadContratos();
              this.clear(form_directive);
            }
          }
        });
  }

  geraContrato(contrato: any): void {
    var id = contrato._orcamento._id;
    if (id) {
      switch (contrato.tipoContrato) {
        case "M":
          window.open(environment.urlAPI + '/ws/reports?rpt=contratoMoveis&sch=Contratos&_id=' + id, '_blank');
          break;
        case "E":
          window.open(environment.urlAPI + '/ws/reports?rpt=contratoEsquadrias&sch=Contratos&_id=' + id, '_blank');
          break;
        case "S":
          window.open(environment.urlAPI + '/ws/reports?rpt=contratoServicos&sch=Contratos&_id=' + id, '_blank');
          break;
      }
    }
  }

  geraFichaEntrega(contrato: any): void {
     var id = contrato._orcamento._id;
     window.open(environment.urlAPI + '/ws/reports?rpt=FichaEntrega&sch=Contratos&_id=' + id, '_blank');
  }


  update(form_directive: any, contratos: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('contratos', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))
        let parse = this.parseForm(obj)
        this.contratos = contratos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive);
      }
    })
  }

  remove(form_directive: any, contratos: any): void {
    let obj = form_directive.value
    this.ApiService.delete('contratos', obj).then(data => {
      if (data) {
        this.contratos = contratos.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })
        this.edit = false;
        this.clear(form_directive);
      }
    });
  }

  select(object: any, form_directive: any): void {
    this.edit = true
    object.edit = this.edit;
    object = this.parseForm(object)
    delete object.$$index;
    if (object._orcamento && object._orcamento._id)
      object._orcamento = object._orcamento._id

    form_directive.form.setValue(object)
    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');
    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset();
    $(':input').val('');
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }

   parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }


}
