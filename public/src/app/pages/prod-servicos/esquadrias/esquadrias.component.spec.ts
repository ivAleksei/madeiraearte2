import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquadriasComponent } from './esquadrias.component';

describe('EsquadriasComponent', () => {
  let component: EsquadriasComponent;
  let fixture: ComponentFixture<EsquadriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsquadriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquadriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
