import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service'

declare var $: any;

@Component({
  selector: 'app-itens',
  templateUrl: './itens.component.html',
  styleUrls: ['./itens.component.scss']
})
export class ItensComponent implements OnInit {

  itens: any = [];
  edit: boolean = false

  constructor(
    private ApiService: ApiService
  ) {
    this.ApiService.checkAuth()
    this.loadItens()
  }

  ngOnInit() {
  }

  loadItens(): void {

    this.ApiService.get('itens')
      .then((data) => {

        this.itens = data.map((d: any) => {
          return this.parseForm(d)
        })
      })
  }

  add(form_directive: any, itens: any): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('itens', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.itens.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, itens: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('itens', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        this.itens = itens.map(function (d: any) {
          if (d._id == form_directive.value._id) {
            d = parse
          }
          return d
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, itens: any): void {

    let obj = form_directive.value

    this.ApiService.delete('itens', obj).then(data => {
      if (data) {
        this.itens = itens.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
    $('.selectpicker').selectpicker('refresh');
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }

  

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }


  select(object: any, form_directive: any): void {
    this.edit = true

    $('#tab-cadastrar').click()
    object = this.parseForm(object)
    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    form_directive.form.setValue(object)
    $('.selectpicker').selectpicker('refresh');
  }
}
