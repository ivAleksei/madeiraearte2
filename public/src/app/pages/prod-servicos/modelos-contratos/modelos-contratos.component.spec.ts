import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelosContratosComponent } from './modelos-contratos.component';

describe('ModelosContratosComponent', () => {
  let component: ModelosContratosComponent;
  let fixture: ComponentFixture<ModelosContratosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelosContratosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelosContratosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
