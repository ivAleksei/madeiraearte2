import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { environment } from '../../../../environments/environment';

declare var $: any;
declare var moment: any

@Component({
  selector: 'app-modeloContrato',
  templateUrl: './modelos-contratos.component.html',
  styleUrls: ['./modelos-contratos.component.scss']
})
export class ModelosContratosComponent implements OnInit {

  modelosContratos: any = [];
  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {
    this.loadModelosContratos();
  }

  public options: Object = {
    language: 'pt',
    height: 600
  }

  ngOnInit() {

  }

  loadModelosContratos(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('modelosContratos')
      .then((data: any) => {
        this.modelosContratos = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }

  add(form_directive: any, modelosContratos: any): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('modelosContratos', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.modelosContratos.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  visualizar(form_directive: any, modelosContratos: any): void {
    let id = form_directive.value._id;
    window.open(environment.urlAPI + '/ws/reports?rpt=avulso&sch=modelosContratos&_id=' + id, '_blank');
  }

  update(form_directive: any, modelosContratos: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('modelosContratos', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        console.log(modelosContratos);

        this.modelosContratos = modelosContratos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, modelosContratos: any): void {

    let obj = form_directive.value

    this.ApiService.delete('modelosContratos', obj).then(data => {
      if (data) {
        this.modelosContratos = modelosContratos.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true

    object = this.parseForm(object)
    delete object.$$index

    form_directive.form.setValue(object)


    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }


}
