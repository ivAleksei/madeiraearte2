import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core'
import { ApiService } from '../../../../shared/services/api.service'
import { MessagesAlertsService } from '../../../../shared/services/messages-alerts.service'
declare var $: any
@Component({
  selector: 'inc-moveis',
  templateUrl: './moveis.component.html',
  styleUrls: ['./moveis.component.scss']
})
export class MoveisComponent implements OnInit {
  ambientes: any[] = []
  itens: any[] = []
  movel: any = {}
  total_itens: number = 0
  edit: boolean = false
  editItem: boolean = false
  item_toEdit: string;
  @Input() orcamento_id: any = ''
  @Input() orcamento_moveis: any = []
  @Input() orcamento_itens: any = []
  orcamento_itensFiltered: any[] = []
  orcamento_itens_details: any[] = []
  subtotal: number;
  @ViewChild('moveisTable') table: any
  constructor(
    private ApiService: ApiService,
    private MessagesAlertsService: MessagesAlertsService
  ) {
    this.loadSelects()
  }
  ngOnInit() {

  }
  loadSelects(): void {
    let selects = ['ambientes', 'itens']
    for (let i in selects) {
      this.ApiService.get(selects[i])
        .then((data: any) => {
          this[selects[i]] = data.map((c: any) => {
            return this.parseForm(c)
          })
        })
    }
  }
  add(form_directive: any, moveis: any[]): void {
    let obj = this.stringForm(form_directive)
    if (this.orcamento_id) {
      this.ApiService.post('orcamentos_moveis', obj, null)
        .then(data => {
          let obj: any = data
          obj._ambiente = this.ambientes.filter(a => {
            return a._id == obj._ambiente
          })[0]
          moveis.push(obj)
          this.clear(form_directive)
        })
    } else {
      obj._id = new Date().getTime()
      obj._ambiente = this.ambientes.filter((a) => {
        return a._id == form_directive.value._ambiente
      })[0]
      moveis.push(this.parseForm(obj))
      this.clear(form_directive)
    }
  }
  update(form_directive: any, orcamentos: any[]): void {
    let obj = this.stringForm(form_directive)
    this.ApiService.put('orcamentos_moveis', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))
        let parse = this.parseForm(obj)
        this.orcamento_moveis = orcamentos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })
        this.edit = false
        this.clear(form_directive)
      }
    })
  }
  remove(obj: any, orcamento_moveis: any[]): void {
    this.ApiService.delete('orcamentos_moveis', obj)
      .then(data => {
        if (data) {
          this.orcamento_moveis = orcamento_moveis.filter(function (f: any) {
            if (f._id != obj._id) return f
          })
        }
      })
  }
  // ======================================================================================== 
  addItem(form_directive: any, orcamento_itens: any[]): void {
    var obj = this.stringForm(form_directive)
    obj._movel = this.movel._id
    form_directive.value.ativo = true
    if (obj && this.orcamento_id) {
      this.ApiService.post('orcamentos_itens', obj, null)
        .then(data => {
          if (data) {
            let parse = JSON.parse(JSON.stringify(data))
            parse = this.parseForm(parse)
            if (parse._id) {
              parse._item = this.itens.filter(i => {
                return i._id == parse._item
              })[0]
              this.orcamento_itens.push(parse)
              this.filterItens({ _id: obj._movel })
              this.clear(form_directive)
            }
          }
        })
    } else {
      obj._id = new Date().getTime()
      let parse = this.parseForm(obj)
      parse._item = this.itens.filter(i => {
        return i._id == parse._item
      })[0]
      this.orcamento_itens.push(parse)
      this.filterItens({ _id: obj._movel })
      this.clear(form_directive)
    }
  }
  updateItem(form_directive: any, orcamento_itens: any[]): void {
    let obj = this.stringForm(form_directive)
    if (obj._orcamento) {
      this.ApiService.put('orcamentos_itens', obj, null).then(data => {
        if (data) {
          let obj = JSON.parse(JSON.stringify(data))
          let parse = this.parseForm(obj)
          this.orcamento_itens = orcamento_itens.map((mod: any) => {
            if (mod._id == form_directive.value._id) {
              mod = parse
            }
            return mod
          })
          this.editItem = false
          this.clear(form_directive)
          this.filterItens({ _id: obj._movel })
        }
      })
    } else {
      this.orcamento_itens = orcamento_itens.map((mod: any) => {
        if (mod._id == form_directive.value._id) {
          mod = obj
        }
        return mod
      })
      this.editItem = false
      this.clear(form_directive)
      this.filterItens({ _id: obj._movel })
    }
  }

  removeItem(form_directive: any, orcamento_itens: any[]): void {
    let obj = this.stringForm(form_directive)
    if (obj._orcamento) {
      this.ApiService.delete('orcamento_itens', obj)
        .then(data => {
          this.orcamento_itens = orcamento_itens.filter(function (f: any) {
            if (f._id != obj._id) return f
          })
          this.filterItens({ _id: obj._movel })
          this.editItem = false
          this.clear(form_directive)
        })
    } else {
      this.orcamento_itens = orcamento_itens.filter(function (f: any) {
        if (f._id != obj._id) return f
      })
      this.filterItens({ _id: obj._movel })
      this.editItem = false
      this.clear(form_directive)
    }
  }
  // ======================================================================================== 
  calculaQntItens(movel: any) {
    return Number(this.orcamento_itens.filter(i => {
      return (i._movel == movel._id)
    }).length)
  }
  calculaSubTotal(movel: any) {
    this.calculaTotal()
    return movel.subtotal || Number(this.orcamento_itens.filter(i => i._movel == movel._id).reduce((prevVal, elem) => prevVal + elem.valores.totais.item, 0).toFixed(2))
  }

  gravaSubTotalMovel(movel: any) {
    if (this.subtotal - movel.subtotal === 0) {
      this.subtotal = null;
      return;
    }
    else
      this.MessagesAlertsService.askConfirmation('Alteração de valor do móvel', 'Deseja alterar o valor do móvel (' + movel.descricao + '), de R$ ' + movel.subtotal.toFixed(2) + ' para R$ ' + this.subtotal.toFixed(2) + '?')
        .then(data => {
          if (data) {
            movel.subtotal = this.subtotal;
            let obj = {
              _id: movel._id,
              subtotal: movel.subtotal
            };

            this.ApiService.put('orcamentos_moveis', obj, null).then(data => {
              if (data) {
                this.subtotal = null;
                this.calculaTotal();
              }
            })
          } else {
            this.subtotal = null;
          }
        })
  }

  setTotal(form_directive: any) {
    if (!form_directive.value._item)
      form_directive.value._item = ''
    form_directive.value.valores.totais.item = form_directive.value.valores.totais.unitario;
    form_directive.form.setValue(form_directive.value)
  }
  editSubtotal(row: any) {
    row.subtotal = this.calculaSubTotal(row);
    this.subtotal = row.subtotal;
    this.item_toEdit = row._id;
  }
  calculaTotal() {
    this.total_itens = this.orcamento_moveis.reduce((prevVal, elem) => {
      if (elem.subtotal)
        return prevVal + elem.subtotal
      else {
        return prevVal + this.orcamento_itens.filter(item => elem._orcamento == item._orcamento).reduce((prevVal, elem) => prevVal + elem.valores.totais.item, 0) || 0;
      }
    }, 0) || 0
    $('#total_itens').html('Total: R$ ' + this.total_itens.toFixed(2))
  }
  calculaValores(form_directive: any) {
    let obj = this.stringForm(form_directive)
    obj.valores.unitario ? obj.valores.unitario : 0
    obj.valores.totais.item ? obj.valores.totais.item : 0
    obj.medidas.altura ? obj.medidas.altura : 0
    obj.medidas.largura ? obj.medidas.largura : 0
    obj.quantidade ? obj.quantidade : 0
    if (!obj._item)
      obj._item = ''
    this.itens.some(i => {
      if (i._id == obj._item) {
        i.valor ? i.valor : 0
        obj.valores.unitario = Number((obj.medidas.altura * obj.medidas.largura * i.valor).toFixed(2))
        obj.valores.totais.unitario = Number((obj.medidas.altura * obj.medidas.largura * i.valor * ((Number(obj.valores.acrescimo) / 100) + 1)).toFixed(2))
        obj.valores.totais.item = Number((obj.quantidade * obj.valores.totais.unitario).toFixed(2))
        return i
      }
    })
    form_directive.form.setValue(obj)
  }
  filterItens(obj: any) {
    this.movel = obj
    this.orcamento_itensFiltered = JSON.parse(JSON.stringify(this.orcamento_itens.filter(i => {
      return i._movel == obj._id
    })))
  }
  select(object: any, form_directive: any): void {
    this.edit = true
    object = this.parseForm(object)
    if (object._ambiente._id)
      object._ambiente = object._ambiente._id

    form_directive.form.setValue(object)
    $('.selectpicker').selectpicker('refresh')
  }
  selectItem(form_directive: any, object: any) {
    this.editItem = true
    object = this.parseForm(object)
    if (object._item._id)
      object._item = object._item._id
    if (!object.valores.totais)
      object.valores.totais = {
        unitario: 0,
        item: 0
      }

    form_directive.form.setValue(object)
    $('.selectpicker').selectpicker('refresh')
  }
  clear(form_directive: any): void {
    this.edit = false
    this.editItem = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh')
  }
  stringForm(form_directive: any): any {
    if (this.orcamento_id)
      form_directive.value._orcamento = this.orcamento_id
    delete form_directive.value.$$index
    return form_directive.value
  }
   parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }


  toggleExpandRow(row, orcamento_itens) {
    this.filterItens(row)
    this.orcamento_itens_details = orcamento_itens.filter(i => {
      return i._movel == row._id
    })
    this.table.rowDetail.toggleExpandRow(row)
  }
  ngAfterViewInit() {
    $('#itens-modal').on('hide.bs.modal', (e) => {
      this.orcamento_itensFiltered = []
    })
  }

  getHeight(row: any, index: number): number {
    return row.someHeight;
  }

}
