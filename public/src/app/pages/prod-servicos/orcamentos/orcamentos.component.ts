import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service'

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-orcamentos',
  templateUrl: './orcamentos.component.html',
  styleUrls: ['./orcamentos.component.scss']
})
export class OrcamentosComponent implements OnInit {

  orcamentos: any = [];
  clientes: any[] = [];
  formapagamentos: any[] = [];
  arquitetos: any[] = [];
  orcamento_moveis: any[] = [];
  orcamento_itens: any = [];

  edit: boolean = false;

  constructor(
    private ApiService: ApiService,
  ) {
    this.loadOrcamentos();
    this.loadSelects();
  }

  ngOnInit() {

  }

  loadOrcamentos(): void {
    var parseForm = this.parseForm;
    this.ApiService.get('orcamentos')
      .then((data: any) => {
        this.orcamentos = data.map(function (c: any) {
          return parseForm(c);
        });
      })
  }


  loadMoveis(orcamento_id: string): void {
    this.ApiService.get('orcamentos_moveis/all' + (orcamento_id ? '?_orcamento=' + orcamento_id : ''))
      .then((data: any) => {
        this.orcamento_moveis = data.map((c: any) => {
          return this.parseForm(c);
        });
      })
  }

  loadItensMoveis(orcamento_id: string): void {
    this.ApiService.get('orcamentos_itens/all' + (orcamento_id ? '?_orcamento=' + orcamento_id : ''))
      .then((data: any) => {
        this.orcamento_itens = data.map((c: any) => {
          return this.parseForm(c);
        });
      })
  }

  loadSelects(): void {
    let selects = ['clientes', 'arquitetos', 'formapagamentos']
    for (let i in selects) {
      this.ApiService.get(selects[i])
        .then((data: any) => {
          this[selects[i]] = data.map((c: any) => {
            return this.parseForm(c)
          })
        })
    }
  }

  add(form_directive: any, array: any[]): void {

    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('orcamentos', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))
            obj = this.parseForm(obj)

            if (this.orcamento_moveis.length)
              this.addMoveis(JSON.parse(JSON.stringify(this.orcamento_moveis)), obj._id);

            obj._cliente = this.clientes.filter(c => {
              return c._id == obj._cliente
            })[0]

            obj._arquiteto = this.arquitetos.filter(c => {
              return c._id == obj._arquiteto
            })[0]

            if (obj._id) {
              this.orcamentos.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  addMoveis(moveis: any[], orcamento_id: string) {
    this.orcamento_moveis = moveis.map(m => {
      m.fake_id = m._id
      m._orcamento = orcamento_id
      m._ambiente = m._ambiente._id

      delete m._id
      delete m.$$index

      return m;
    })
    var orcamento_itens = this.orcamento_itens
    this.ApiService.post('orcamentos_moveis/ar', this.orcamento_moveis, null)
      .then(data => {
        if (orcamento_itens.length)
          this.addItens(JSON.parse(JSON.stringify(orcamento_itens)), orcamento_id, data);
        this.orcamento_moveis = [];
      })
  }

  addItens(itens: any[], orcamento_id: string, moveis_cadastrados: any[]) {
    this.orcamento_itens = itens.map(m => {
      m.fake_id = m._id
      m._orcamento = orcamento_id
      m._item = m._item._id

      m._movel = moveis_cadastrados.filter(m2 => {
        return m2.fake_id == m._movel
      }).map(m => {
        return m._id
      })[0]

      delete m.$$index
      delete m._id

      return m;
    })
    this.ApiService.post('orcamentos_itens/ar', this.orcamento_itens, null)
      .then(data => { })
  }

  update(form_directive: any, orcamentos: any, array: string): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('orcamentos', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)

        this.orcamentos = orcamentos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        }).filter(mod => {
          return mod.ativo
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, orcamentos: any, array: string): void {

    let obj = form_directive.value

    this.ApiService.deleteQuery('orcamentos_moveis/q?_orcamento=' + obj._id, obj, false)
    this.ApiService.deleteQuery('orcamentos_itens/q?_orcamento=' + obj._id, obj, false)

    this.ApiService.delete('orcamentos', obj)
      .then(data => {
        if (data) {
          this.orcamentos = orcamentos.filter(function (f: any) {
            if (f._id != form_directive.value._id) return f
          })

          this.edit = false
          this.clear(form_directive)
        }
      })
  }

  select(object: any, form_directive: any): void {
    this.edit = true

    object = this.parseForm(object)

    object._cliente = object._cliente._id
    object._arquiteto = object._arquiteto._id
    this.loadMoveis(object._id)
    this.loadItensMoveis(object._id)

    delete object.$$index

    if (object._formapagamento && object._formapagamento._id)
      object._formapagamento = object._formapagamento._id

    form_directive.form.setValue(object)
    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    $('.selectpicker').selectpicker('refresh');

    $('#tab-cadastrar').click()
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()

    this.orcamento_moveis = []
    this.orcamento_itens = []

    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
    $('.selectpicker').selectpicker('refresh')
  }

  stringForm(form_directive: any): any {
    delete form_directive.value.$$index

    if ($('input[name="prevista"]').val() != '')
      form_directive.value.entrega.datas.prevista = moment($('input[name="prevista"]').val(), "DD/MM/YYYY").format('YYYY-MM-DD')
    if ($('input[name="definitiva"]').val() != '')
      form_directive.value.entrega.datas.definitiva = moment($('input[name="definitiva"]').val(), "DD/MM/YYYY").format('YYYY-MM-DD')


    return form_directive.value
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit


    if (parse.entrega) {
      if (parse.entrega.datas.prevista.length > 10)
        parse.entrega.datas.prevista = moment(parse.entrega.datas.prevista).add(1, 'd').format('DD/MM/YYYY')

      if (parse.entrega.datas.definitiva.length > 10)
        parse.entrega.datas.definitiva = moment(parse.entrega.datas.definitiva).add(1, 'd').format('DD/MM/YYYY')
    }
    return parse
  }


}
