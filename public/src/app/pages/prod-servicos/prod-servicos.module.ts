import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';

import { OrcamentosComponent } from './orcamentos/orcamentos.component';

import { EsquadriasComponent } from './esquadrias/esquadrias.component';
import { ItensComponent } from './itens/itens.component';
import { ModelosContratosComponent } from './modelos-contratos/modelos-contratos.component';
import { MoveisComponent } from './orcamentos/moveis/moveis.component';
import { ContratosComponent } from './contratos/contratos.component';


const PROD_SERVICOS_ROUTES = [
  { path: 'itens', component: ItensComponent },
  { path: 'orcamentos', component: OrcamentosComponent },
  { path: 'esquadrias', component: EsquadriasComponent },
  { path: 'modeloscontratos', component: ModelosContratosComponent },
  { path: 'contratos', component: ContratosComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild(PROD_SERVICOS_ROUTES),
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot(),
  ],
  declarations: [
    OrcamentosComponent,
    EsquadriasComponent,
    ItensComponent,
    ModelosContratosComponent,
    MoveisComponent,
    ContratosComponent,
  ],
  exports: [
    OrcamentosComponent,
    EsquadriasComponent,
    ItensComponent,
    ModelosContratosComponent,
    ContratosComponent
  ]
})
export class ProdServicosModule {}
