import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { ApiService } from '../../../shared/services/api.service'

declare var $: any

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.scss']
})
export class ModulosComponent implements OnInit {
  modulos: any = [];
  edit: boolean = false

  constructor(
    private ApiService: ApiService
  ) {
    this.ApiService.checkAuth()
    this.loadModulos()
  }

  ngOnInit() {
  }

  loadModulos(): void {

    this.ApiService.get('modulos/all')
      .then((data) => {

        this.modulos = data.map((mod: any) => {
          return this.parseForm(mod)
        })
      })
  }

  add(form_directive: any, modulos: any): void {

    var obj = this.stringForm(form_directive)
    
    form_directive.value.ativo = true

    if (obj)
      this.ApiService.post('modulos', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.modulos.push(obj)
              this.clear(form_directive)
            }
          }
        })
  }

  update(form_directive: any, modulos: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('modulos', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        this.modulos = modulos.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, modulos: any): void {

    let obj = form_directive.value

    this.ApiService.delete('modulos', obj).then(data => {
      if (data) {
        this.modulos = modulos.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    // $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }

  select(object: any, form_directive: any): void {
    this.edit = true

    $('#tab-cadastrar').click()
    object = this.parseForm(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    form_directive.form.setValue(object)
  }
}
