import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { ApiService } from '../../../shared/services/api.service'

declare var $: any

@Component({
  selector: 'app-paginas',
  templateUrl: './paginas.component.html',
  styleUrls: ['./paginas.component.scss']
})
export class PaginasComponent implements OnInit {
  paginas: any[] = []
  modulos: any[] = []
  orcamentos: any[] = []
  edit: boolean = false

  constructor(
    private ApiService: ApiService
  ) {
    this.ApiService.checkAuth()
    this.loadPaginas()
    this.loadModulos()
  }

  ngOnInit() {
  }

  getRowClass(row) {
    //return  row.$$index%2 === 0? 'datatable-row-odd':'datatable-row-even'
  return {
    'datatable-row-odd': row.$$index%2 === 0,
    'datatable-row-even': row.$$index%2 !== 0,
  }
}

  loadPaginas(): void {

    this.ApiService.get('paginas/all')
      .then((data) => {

        this.paginas = data.map((mod: any) => {
          return this.parseForm(mod)
        })
      })
  }

  loadModulos(): void {

    this.ApiService.get('modulos/all')
      .then((data) => {

        this.modulos = data.map((mod: any) => {
          return this.parseForm(mod)
        })
      })
  }


  add(form_directive: any, paginas: any): void {

    this.stringForm(form_directive)

    form_directive.value.ativo = true

    var obj = this.stringForm(form_directive)

    if (obj) {
      this.ApiService.post('paginas', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)

            if (obj._id) {
              this.paginas.push(obj)
              this.clear(form_directive)
            }
          }
        })
    }
  }

  update(form_directive: any, paginas: any): void {
    let obj = form_directive.value._modulo ? this.stringForm(form_directive) : form_directive.value

    this.ApiService.put('paginas', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)

        this.paginas = paginas.map(function (page: any) {
          if (page._id == form_directive.value._id) {
            page = parse
          }
          return page
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  remove(form_directive: any, paginas: any): void {

    let obj = form_directive.value

    this.ApiService.delete('paginas', obj).then(data => {
      if (data) {
        this.paginas = paginas.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }


  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')
    $('#tab-consultar').click()
    $('.selectpicker').selectpicker('refresh')
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }
 

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    
    if (parse._modulo && parse._modulo._id)
      parse._modulo = parse._modulo._id
    delete parse.$$index
    delete parse.edit
    return parse
  }

  select(object: any, form_directive: any): void {
    this.edit = true

    $('#tab-cadastrar').click()
    object = this.parseForm(object)

    $('.form-control').parents('div.fg-line').addClass('fg-toggled')
    form_directive.form.setValue(object)

    $('.selectpicker').selectpicker('refresh')
  }

  recalcula(){
    console.log('recalcula')
  }

}
