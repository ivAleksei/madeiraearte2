import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from "../../shared/shared.module";


import { UsuariosComponent } from './usuarios/usuarios.component';
import { PaginasComponent } from './paginas/paginas.component';
import { ModulosComponent } from './modulos/modulos.component';

const SYSADMIN_ROUTES = [
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'paginas', component: PaginasComponent },
  { path: 'modulos', component: ModulosComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SYSADMIN_ROUTES)
  ],
  declarations: [
    UsuariosComponent,
    PaginasComponent,
    ModulosComponent,
  ],
  exports: [
    UsuariosComponent,
    PaginasComponent,
    ModulosComponent
  ] 
})
export class SysAdminModule { }
