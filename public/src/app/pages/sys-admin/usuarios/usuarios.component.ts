import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core'
import { ApiService } from '../../../shared/services/api.service'
import { environment } from '../../../../environments/environment';
import { Md5 } from 'ts-md5/dist/md5';

declare var $: any

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  edit: boolean = false
  usuario: any = {};
  usuarios: any[] = []
  paginas: any[] = []
  modulos: any[] = []
  files: any[] = []

  columns: any[] = []

  @ViewChild('ativoTmpl') ativoTmpl: TemplateRef<any>
  @ViewChild('editTmpl') editTmpl: TemplateRef<any>
  @ViewChild('checkTmpl') checkTmpl: TemplateRef<any>

  constructor(
    private ApiService: ApiService
  ) {
    this.ApiService.checkAuth()
    this.loadUsuarios()
    this.loadModulos()
    this.loadPaginas()
  }

  ngOnInit() {
    this.columns = [
      { name: 'Login' },
      { name: 'Nome' },
      {
        cellTemplate: this.ativoTmpl,
        name: 'Ativo'
      },
      {
        cellTemplate: this.checkTmpl,
        name: 'Permissoes'
      },
      {
        cellTemplate: this.editTmpl,
        name: 'Editar'
      }
    ]
  }

  add(form_directive: any, usuarios: any[]): void {

    let obj = this.stringForm(form_directive)

    form_directive.value.ativo = true

    if (obj) {
      this.ApiService.post('usuarios', obj, this.files).then(data => {
        if (data) {
          let obj = this.parseForm(data)
          if (obj._id) {
            this.usuarios.push(obj)
            this.clear(form_directive)
          }
        }
      })
    }
  }

  select(usuario: any, form_directive: any): void {
    this.edit = true

    $('#tab-cadastrar').click()
    $('#usuarioForm').find('.fg-line').addClass('fg-toggled')

    let obj = this.parseForm(usuario)
    obj.password = '********'
    obj.password_confirma = '********'

    form_directive.form.setValue(obj)
  }

  update(form_directive: any, usuarios: any[]): void {
    let obj = form_directive.value.password ? this.stringForm(form_directive) : form_directive.value;

    if (typeof obj.user_pic != 'object') {
      delete obj.user_pic;
    }

    this.ApiService.put('usuarios', obj, this.files).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data));

        let parse = this.parseForm(obj);
        this.usuarios = usuarios.map(function (u) {
          if (u._id == form_directive.value._id) {
            u = parse;
          }
          return u;
        })

        this.edit = false;
        this.clear(form_directive);
      }
    });
  }

  remove(form_directive: any, usuarios: any): void {

    let obj = form_directive.value

    this.ApiService.delete('usuarios', obj).then(data => {
      if (data) {
        this.usuarios = usuarios.filter(function (f: any) {
          if (f._id != form_directive.value._id) return f
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  check(usuario: any) {
    $('#tab-autorizar').click();

    this.modulos.map((m: any) => {
      m.check = usuario._modulos.some((_m: any) => {
        return _m == m._id;
      })
      return m;
    })

    this.paginas.map((p: any) => {
      p.check = usuario._paginas.some((_p: any) => {
        return _p == p._id;
      })
      return p;
    })

    this.usuario = usuario;
  }

  gravarPermissoes(form_directive: any, usuarios: any[]) {
    let obj:any = {
      _id: this.usuario._id,
      _modulos: this.modulos.filter((m: any) => {
        return m.check;
      }).map((m: any) => {
        return m._id;
      }),
      _paginas : this.paginas.filter((p: any) => {
        return p.check;
      }).map((p: any) => {
        return p._id;
      })
    }

    this.ApiService.put('usuarios', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data));

        let parse = this.parseForm(obj);

        this.usuarios = usuarios.map(function (u) {
          if (u._id == obj._id) {
            u = parse;
          }
          return u;
        })

        this.edit = false;
        this.clear(form_directive);
      }
    });
  }


  loadUsuarios(): void {

    this.ApiService.get('usuarios/all')
      .then((data) => {

        this.usuarios = data.map((mod: any) => {
          return this.parseForm(mod)
        })

      })
  }

  loadModulos(): void {

    this.ApiService.get('modulos/all')
      .then((data) => {

        this.modulos = data.map((mod: any) => {
          return this.parseForm(mod)
        })

      })
  }

  loadPaginas(): void {

    this.ApiService.get('paginas/all')
      .then((data) => {
        this.paginas = data.map((obj: any) => {
          if (obj._modulo && obj._modulo._id)
            obj._modulo = obj._modulo._id;
          return this.parseForm(obj)
        })

      })
  }

  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
    $('.form-control').parents('div.fg-line').removeClass('fg-toggled')

    $('#tab-consultar').click()
  }

  stringForm(form_directive: any): any {
    delete form_directive.value.password_confirma
    delete form_directive.value._paginas
    delete form_directive.value._modulos

    form_directive.value.login_exib = form_directive.value.login
    form_directive.value.login = Md5.hashStr(form_directive.value.login)
    form_directive.value.password = Md5.hashStr(form_directive.value.login + form_directive.value.password)

    return form_directive.value
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }
}