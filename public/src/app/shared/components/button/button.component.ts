import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'action-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() icone:any;
  constructor() { }

  @Output() public edit: EventEmitter<any[]> = new EventEmitter();

  ngOnInit() {
  }

  onClick(): void {
    this.edit.next();
  }
}
