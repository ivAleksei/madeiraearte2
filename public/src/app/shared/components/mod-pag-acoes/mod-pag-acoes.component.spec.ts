import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModPagAcoesComponent } from './mod-pag-acoes.component';

describe('ModPagAcoesComponent', () => {
  let component: ModPagAcoesComponent;
  let fixture: ComponentFixture<ModPagAcoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModPagAcoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModPagAcoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
