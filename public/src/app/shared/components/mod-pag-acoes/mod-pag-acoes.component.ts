import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mod-pag-acoes',
  templateUrl: './mod-pag-acoes.component.html',
  styleUrls: ['./mod-pag-acoes.component.scss']
})
export class ModPagAcoesComponent implements OnInit {

  @Input() modulos:any;
  @Input() paginas:any;
  @Input() acoes:any;

  constructor() { }

  ngOnInit() {
  }

  selectModulo(mod:any, paginas:any[]){
    this.paginas = paginas.map((p:any)=>{
      if(p && (p._modulo == mod._id))
        p.check = mod.check;
      return p;
    })
  }
}

