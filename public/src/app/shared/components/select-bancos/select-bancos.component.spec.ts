import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBancosComponent } from './select-bancos.component';

describe('SelectBancosComponent', () => {
  let component: SelectBancosComponent;
  let fixture: ComponentFixture<SelectBancosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBancosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBancosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
