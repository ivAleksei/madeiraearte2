import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'select-bancos',
  templateUrl: './select-bancos.component.html',
  styleUrls: ['./select-bancos.component.scss']
})
export class SelectBancosComponent implements OnInit {

  @Input()
  cod_febraban:string;

  constructor() { }

  ngOnInit() {
  }

}
