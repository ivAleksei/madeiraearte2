import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleAtivoComponent } from './toggle-ativo.component';

describe('ToggleAtivoComponent', () => {
  let component: ToggleAtivoComponent;
  let fixture: ComponentFixture<ToggleAtivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleAtivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleAtivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
