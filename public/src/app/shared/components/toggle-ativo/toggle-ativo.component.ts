import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../services/api.service'

declare var $: any;

@Component({
  selector: 'toggle-ativo',
  templateUrl: './toggle-ativo.component.html',
  styleUrls: ['./toggle-ativo.component.scss']
})
export class ToggleAtivoComponent implements OnInit {

  @Input() Form: any;
  @Input() row: any;
  @Input() update: any;
  @Input() data: any;
  @Input() parseForm: any;

  constructor(
    private ApiService: ApiService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      $('#ts' + this.row._id).prop('checked', this.row.ativo);
    }, 100)

  }

  toggle(object: any, form_directive: any) {
    this.row.ativo = !this.row.ativo;
    $('#ts' + this.row._id).prop('checked', !object.ativo);
    object = this.parseForm(object);

    form_directive.form.setValue(object);

    for (let k in form_directive.value) {
      if (k != '_id' && k != 'ativo')
        delete form_directive.value[k]
    }
    this.update(form_directive, this.data);
  }

  stringForm(form_directive: any): any {
    return form_directive.value;
  }

  clear(form_directive: any): void {
    form_directive.form.reset();
  }
}
