import { Directive, ElementRef, Input } from '@angular/core';

declare var $: any;

@Directive({
  selector: '.selectpicker',
  host: {
  }
})
export class BootstrapSelectDirective {

  @Input() data: any;
  @Input() value: any;
  @Input() label: any;
  @Input() label2: any;

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  ngOnInit() {
    if (this.data) {
      let str: string = '';
      for (let i in this.data) {
        if (this.label2) {
          str = '<option value="' + this.data[i][this.value || '_id' || ''] + '">' + this.data[i][this.label || 'nome'][this.label2] + '</option>';
        } else {
          str = '<option value="' + this.data[i][this.value || '_id' || ''] + '">' + this.data[i][this.label || 'nome'] + '</option>';
        }
      
        $(this.elem).html($(this.elem).html() + str);
      }
      $(this.elem).selectpicker();
    } else {
      $(this.elem).selectpicker();
    }
  }

}
