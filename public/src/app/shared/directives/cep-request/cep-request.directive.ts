import { Directive, ElementRef, NgZone, EventEmitter, Output } from '@angular/core';
import { ApiService } from '../../services/api.service'

declare var $: any;

@Directive({
  selector: '[CepRequest]',
  host: {
    '(blur)': 'buscaCEP()'
  }
})
export class CepRequestDirective {

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter(false);

  constructor(
    private ApiService: ApiService,
    private el: ElementRef,
    private _ngZone: NgZone
  ) { }

  elem: any = this.el.nativeElement;

  buscaCEP() {
    this.ApiService.getExternal('http://viacep.com.br/ws/' + this.elem.value + '/json/')
      .then((data) => {
        for (let d in data) {
          $('input[name="' + d + '"]').val(data[d]);
          if (data[d])
            $('input[name="' + d + '"]').parents('.fg-line').addClass('fg-toggled');
        }
      })
  }
}
