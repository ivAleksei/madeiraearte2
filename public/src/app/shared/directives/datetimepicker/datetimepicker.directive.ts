import { Directive, ElementRef } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[DateTimePicker]',
  host: {

  }
})
export class DatetimepickerDirective {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;
  

  ngOnInit() {
    
    //Date Time Picker
    if ($(this.elem).hasClass('date-time-picker')) {
      $(this.elem).datetimepicker({
        locale: 'pt-br'
      });
    }

    //Time
    if ($(this.elem).hasClass('time-picker')) {
      $(this.elem).datetimepicker({
        locale: 'pt-br',
        format: 'LT'
      });
    }

    if ($(this.elem).hasClass('date-picker')) {
      $(this.elem).datetimepicker({
        locale: 'pt-br',
        format: 'DD/MM/YYYY'
      });
      $(this.elem).on('dp.hide', function () {
        $(this).closest('.dtp-container').removeClass('fg-toggled');
        $(this).blur();
      })
    }
  }
}
