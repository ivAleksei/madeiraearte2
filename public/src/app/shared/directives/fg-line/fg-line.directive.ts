import { Directive, ElementRef, Renderer } from "@angular/core";

declare var $: any;

@Directive({
  selector: '.form-control',
  host: {
    '(blur)': 'onBlur()',
    '(focus)': 'onFocus()'
  }
})
export class FgLineDirective {

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) { }

  elem: any = this.el.nativeElement;

  onFocus() {
    $(this.elem).parents('div.fg-line').addClass('fg-toggled');

  }

  onBlur() {    
    if (!this.elem.value)
    $(this.elem).parents('div.fg-line').removeClass('fg-toggled');
  }

  ngOnInit() {
    if (this.elem.value)
      this.onBlur();
  }

}
