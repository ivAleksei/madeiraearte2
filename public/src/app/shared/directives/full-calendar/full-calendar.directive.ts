import { Directive, ElementRef, Renderer, Input } from "@angular/core";
import { ApiService } from '../../../shared/services/api.service'

declare var $: any;
declare var moment: any;

@Directive({
  selector: '[FullCalendar]'
})
export class FullCalendarDirective {
  constructor(
    private ApiService: ApiService,
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  @Input() eventos: any[] = null
  @Input() defaultView: any
  today: any = new Date()

  ngOnInit() {
    this.start();
  }

  start() {
    if(this.defaultView == 'listMonth')
      this.eventos = this.eventos.filter(m=>new Date(m.start) >= new Date())
    $(this.elem).fullCalendar({
      contentHeight: 'auto',
      theme: false,
      header: {
        right: 'next',
        center: 'title, ',
        left: 'prev'
      },      
      locale: 'pt-br',
      defaultView: this.defaultView,
      defaultDate: this.today,
      editable: true,
      events: this.eventos,
      handleWindowResize: false,
      height: 468,
      noEventsMessage: "Nenhum evento cadastrado.", 
      dayClick: (date) => this.dayClick(date),
      eventClick: (event, element) => this.eventClick(event, element)
    });
  }

  dayClick(date: any) {
    var isoDate = moment(date).format('DD/MM/YYYY HH:mm');

    $('#modal-new-event').modal('show');
    $('#event-name').val('');
    $('#new-event-start').val(isoDate);
    $('#new-event-end').val(isoDate);
  }

  eventClick(event: any, element: any) {
    $('.edit-event-id').val(event.id);
    $('.edit-event-title').val(event.title);
    $('.edit-event-description').val(event.description);
    $('#modal-edit-event input[value=' + event.className + ']').prop('checked', true);
    $('#modal-edit-event').modal('show');
  }
  ngAfterViewInit() {
    $('.fc-scroller').perfectScrollbar();
  }
}

