import { Directive, ElementRef, OnInit } from '@angular/core';

declare var $:any;

@Directive({
  selector: '.hi-trigger',
  host:{
    '(click)':'onClick()'
  }
})
export class HiTriggerDirective implements OnInit {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  ngOnInit(){
  }

  onClick(){
    $('#sidebar').toggleClass('toggled')
  }

}
