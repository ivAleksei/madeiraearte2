import { Directive, ElementRef, Renderer } from '@angular/core';

declare var $: any

@Directive({
  selector: '[currencyMask]',
  host: {
    '(focus)': 'onFocus()',
    '(blur)': 'onBlur()'
  }
})
export class InputCurrencyMaskDirective {

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) { }

  valor_temp: string
  elem: any = this.el.nativeElement;

  onFocus() {
    this.valor_temp = this.elem.value
    this.elem.value = Number(this.elem.value) == 0 ? '' : this.elem.value
    this.elem.value = this.elem.value == '0,00' ? '' : this.elem.value
  }

  onBlur(){
    if(!this.elem.value)
      this.elem.value = this.valor_temp
  }

}
