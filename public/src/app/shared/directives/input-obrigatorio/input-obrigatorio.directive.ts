import { Directive, ElementRef } from '@angular/core';
import { MessagesAlertsService } from '../../../shared/services/messages-alerts.service'

declare var $:any;

@Directive({
  selector: '[InputObrigatorio]',
  host: {
    '(blur)':'onBlur()'
  }
})
export class InputObrigatorioDirective {

  elem: any = this.el.nativeElement;

  constructor(
    private el: ElementRef,
    private MessagesAlertsService: MessagesAlertsService
  ) { }

  onBlur(){
    $(this.elem.parentElement).removeClass('has-error has-sucess has-warning')
      if(this.elem.value == ''){
        this.MessagesAlertsService.growl('POR FAVOR, PREENCHA OS CAMPOS OBRIGATÓRIOS EM DESTAQUE', 'error')
        $(this.elem.parentElement).addClass('has-error');
      }
  }  
}
