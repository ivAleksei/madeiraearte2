import { Directive, ElementRef, OnInit } from '@angular/core';

declare var $:any;

@Directive({
  selector: '.list-group-modulos',
  host:{
    '(click)':'onClick()'
  }
})
export class ListGroupDirective implements OnInit {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  ngOnInit(){
  }

  onClick(){
    $(this.elem).parents('.modpag-accordion').find('ul').slideToggle('fast');
  }

}
