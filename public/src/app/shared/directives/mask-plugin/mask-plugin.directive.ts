/**
 * Diretiva de máscara genérica em campo de texto.
 *
 * @author Márcio Casale de Souza <contato@kazale.com>
 * @since 9.9.4
 */

import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import {
  NG_VALUE_ACCESSOR, ControlValueAccessor
} from '@angular/forms';

declare var $: any;

@Directive({
  selector: '[Mask]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: MaskPluginDirective,
    multi: true
  }]
})
export class MaskPluginDirective implements ControlValueAccessor {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  onTouched: any;
  onChange: any;
  @Input('Mask') kzMask: string;

  writeValue(value: any): void {
     if (value) {
      this.el.nativeElement.value = this.aplicarMascara(value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    var pattern = this.getPattern()
    var valor = $event.target.value.replace(/\D/g, '');
    var pad = pattern.replace(/9/g, '_');
    var valorMask = valor + pad.substring(0, pad.length - valor.length);

    // retorna caso pressionado backspace
    if ($event.keyCode === 8) {
      this.onChange(valor);
      return;
    }

    var valorMaskPos = 0;
    valor = '';
    for (var i = 0; i < pattern.length; i++) {
      if (isNaN(parseInt(pattern.charAt(i)))) {
        valor += pattern.charAt(i);
      } else {
        valor += valorMask[valorMaskPos++];
      }
    }

    if (valor.indexOf('_') > -1) {
      valor = valor.substr(0, valor.indexOf('_'));
    }

    if (valor.length <= pad.length) {
      this.onChange(valor);
    }

    $event.target.value = valor;
  }

  @HostListener('focus', ['$event'])
  onFocus($event: any) {
    //console.log($event)
    //  var pattern = this.getPattern()
    //  if ($event.target.value.length === pattern.length) {
    //    return;
    //  }

  }
  // @HostListener('blur', ['$event'])
  // onBlur($event: any) {

  //   //  var pattern = this.getPattern()
  //   //  if ($event.target.value.length === pattern.length) {
  //   //    return;
  //   //  }

  // }

  getPattern(): string {
    switch ($(this.elem).attr('data-mask')) {
      // case 'date':
      //   return '99/99/9999'
      case 'time':
        return '99:99:99'
      case 'date_time':
        return '99/99/9999 99:99:99'
      case 'cep':
        return '99999-999'
      case 'phone':
        return '9999-9999'
      case 'cellphone':
        return '99999-9999'
      case 'phone_with_ddd':
        'return (99) 99999-9999'
      case 'cellphone_with_ddd':
        return '(99) 99999-9999'
      case 'cpf':
        return '999.999.999-99'
      case 'cnpj':
        return '99.999.999/9999-99'
      case 'bank-account':
        return '99.999-9'
      case 'agency':
        return '999999-9'
      default:
        return $(this.elem).attr('data-mask')
    }
  }

  /**
   * Aplica a máscara a determinado valor.
   *
   * @param string valor
   * @return string
   */
  aplicarMascara(valor: string): string {
    valor = valor.replace(/\D/g, '');
    let pad = this.kzMask.replace(/\D/g, '').replace(/9/g, '_');
    let valorMask = valor + pad.substring(0, pad.length - valor.length);
    let valorMaskPos = 0;
    
    valor = '';
    for (let i = 0; i < this.kzMask.length; i++) {
      if (isNaN(parseInt(this.kzMask.charAt(i)))) {
        valor += this.kzMask.charAt(i);
      } else {
        valor += valorMask[valorMaskPos++];
      }
    }
    
    if (valor.indexOf('_') > -1) {
      valor = valor.substr(0, valor.indexOf('_'));
    }

    return valor;
  }

}