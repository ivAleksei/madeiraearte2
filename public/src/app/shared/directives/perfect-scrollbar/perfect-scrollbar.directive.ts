import { Directive, ElementRef, Renderer } from "@angular/core";

declare var $:any;

@Directive({
  selector: '[perfectScrollbar]'
})
export class PerfectScrollbarDirective {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  ngOnInit() {
    $(this.elem).perfectScrollbar();
  }

}

