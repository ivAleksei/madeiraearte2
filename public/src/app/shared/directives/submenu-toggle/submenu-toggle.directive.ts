import { Directive, ElementRef, Renderer } from "@angular/core";

declare var $:any;

@Directive({
  selector: '[data-ma-action="submenu-toggle"]',
  host: {
    '(click)': 'onClick()'
  }
})
export class SubmenuToggleDirective {

  constructor(
    private el: ElementRef
  ) { }

  elem: any = this.el.nativeElement;

  onClick() {
    $(this.elem).next().slideToggle(200);
    $(this.elem).parent().toggleClass('toggled');
  }

  ngOnInit() {
    $(this.elem).next().slideUp();
  }

}
