import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MessagesAlertsService } from './messages-alerts.service';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var $: any;
declare var window: any;

@Injectable()
export class ApiService {

  constructor(
    private http: Http,
    private MessagesAlertsService: MessagesAlertsService,
    private router: Router
  ) { }

  urlHost: string = environment.urlAPI;

  getHeaders(): Headers {

    let headers = new Headers();
    let sessionToken = window.sessionStorage.getItem('x-access-token');
    let localToken = window.localStorage.getItem('x-access-token');

    headers.append('x-access-token', sessionToken || localToken);
    return headers;
  }

  checkToken(): Promise<any[]> {

    let headers = this.getHeaders();

    return Promise.resolve(this.http.get(this.urlHost + '/ws/auth', { headers: headers })
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        return res.status;
      }));
  }


  showLoader(): void {
    $('#service-loader').css({ display: 'block' }).find('.progress-bar').css({ width: '0%' })
    $('#service-loader').parent().css({ display: 'block' })
  }
  completeLoad(): void {
    $('#service-loader').find('.progress-bar').css({ width: '100%', 'background-color': '#009688' })
  }
  errorLoad(): void {
    $('#service-loader').find('.progress-bar').css({ width: '100%', 'background-color': '#F44336' })
  }

  hideLoader(): void {
    setTimeout(() => {
      $('#service-loader').css({ display: 'none' })
    }, 500)
  }



  get(schema: string): Promise<any[]> {
    this.showLoader()
    let headers = this.getHeaders();

    return Promise.resolve(this.http.get(this.urlHost + '/ws/' + schema, { headers: headers })
      .toPromise()
      .then((res) => {
        this.completeLoad()
        return JSON.parse(res.text())
      })
      .catch((res) => {
        this.msgRetorno(schema, res, 'get')
        return {};
      }));
  }

  getInfoApp(schema: string): Promise<any> {

    let headers = this.getHeaders();

    return Promise.resolve(this.http.get(this.urlHost + '/ws/info_' + schema, { headers: headers })
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        return {};
      }));
  }

  getResource(data: string): Promise<any[]> {

    return Promise.resolve(this.http.get(this.urlHost + '/ws/resources?data=' + data)
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        return [];
      }));
  }

  getExternal(url: string): Promise<any[]> {

    return Promise.resolve(this.http.get(url)
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        return [];
      }));
  }

  post(schema: string, body: any, files: any): Promise<any> {

    if (body._id == null)
      delete body._id

    let headers = this.getHeaders();

    var fd = new FormData();

    if (files)
      for (let f of files) {
        if (f)
          fd.append(f.name, f, f.name);
      }

    fd.append('data', JSON.stringify(body));

    return Promise.resolve(this.http.post(this.urlHost + '/ws/' + schema, fd, { headers: headers })
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        this.msgRetorno(schema, res, 'post')
        return {};
      })
    );
  }

  put(schema: string, body: any, files: any): Promise<any> {
    let headers = this.getHeaders();

    var fd = new FormData();

    if (files)
      for (let f of files) {
        if (f)
          fd.append(f.name, f, f.name);
      }

    fd.append('data', JSON.stringify(body));

    return Promise.resolve(this.http.put(this.urlHost + '/ws/' + schema + '/' + body._id, fd, { headers: headers })
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        this.msgRetorno(schema, res, 'put')
      })
    );
  }

  delete(schema: string, body: any): Promise<any> {
    let headers = this.getHeaders();
    return this.MessagesAlertsService.deleteConfirmation()
      .then(data => {
        if (data)
          return Promise.resolve(this.http.delete(this.urlHost + '/ws/' + schema + '/' + body._id, { headers: headers })
            .toPromise()
            .then((res) => {
              return JSON.parse(res.text())
            })
            .catch((res) => {
              this.msgRetorno(schema, res, 'delete')
            })
          );
      })
  }

  deleteQuery(schema: string, body: any, confirmation: boolean): Promise<any> {
    let headers = this.getHeaders();
    if (confirmation) {
      return this.MessagesAlertsService.deleteConfirmation()
        .then(data => {
          if (data)
            return Promise.resolve(this.http.delete(this.urlHost + '/ws/' + schema, { headers: headers })
              .toPromise()
              .then((res) => {
                return JSON.parse(res.text())
              })
              .catch((res) => {
                this.msgRetorno(schema, res, 'delete')
              })
            );
        })
    } else {

      return Promise.resolve(this.http.delete(this.urlHost + '/ws/' + schema, { headers: headers })
        .toPromise()
        .then((res) => {
          return JSON.parse(res.text())
        })
        .catch((res) => {
          this.msgRetorno(schema, res, 'delete')
        }))

    }

  }

  deleteArray(schema: string, body: any): Promise<any[]> {
    let headers = this.getHeaders();

    var fd = new FormData();

    fd.append('data', JSON.stringify(body));

    return Promise.resolve(this.http.post(this.urlHost + '/ws/' + schema + '/del', fd, { headers: headers })
      .toPromise()
      .then((res) => {
        return JSON.parse(res.text())
      })
      .catch((res) => {
        this.msgRetorno(schema, res, 'deleteArray')
      })
    );
  }

  checkAuth() {

    let headers = this.getHeaders();


    if (!window.sessionStorage.getItem('user_id')) {
      console.log('nao _id')
      this.MessagesAlertsService.growl('ACESSO NAO PERMITIDO', 'error');
      this.router.navigateByUrl('/login');
      return;
    }

    return Promise.resolve(this.http.get(this.urlHost + '/ws/usuarios/_info/' + window.sessionStorage.getItem('user_id'), { headers: headers })
      .toPromise()
      .then((res) => {
        let user_info = JSON.parse(res.text())

        var url = this.router.url;

        if (url == '/dashboard') {
          return JSON.parse(res.text());
        }

        var route = url.split('/').slice(2);


        var modAuth = user_info.modulosAutorizados.some(function (m: any) {
          if (m.codigo == route[0])
            return m;
        })

        var pagAuth = user_info.modulosAutorizados.some(function (m: any) {
          return m.paginas.some(function (p: any) {
            if (p.codigo == route[1])
              return p;
          });
        })

        if (!modAuth || !pagAuth) {
          console.log('nao _id')
          this.MessagesAlertsService.growl('ACESSO NAO PERMITIDO', 'error');
          this.router.navigateByUrl('/dashboard');
        }
        return JSON.parse(res.text())
      })
      .catch((res) => {
        return {};
      }));
  }

  // FUNÇÃO RECURSIVA - CUIDADO AO MEXER
  populate(obj, c) {
    for (let f in obj)
      if (String(obj[f])) {
        if (typeof obj[f] == 'object') {
          this.populate(obj[f], (c ? c + '.' + f : f))
        }
      } else {
        delete obj[f];
      }
  }

  msgRetorno(schema: string, res: any, method: string): any {
    var str: string;
    var typeMessage: string;
    this.errorLoad();


    switch (res.status) {
      case 400:
        typeMessage = 'error'
        str = JSON.parse(res.text()).strMessage.toLocaleUpperCase()
        break;
      case 401:
        typeMessage = 'inverse'
        str = 'OPERAÇÃO NÃO AUTORIZADA'
        break;
      case 403:
        typeMessage = 'inverse'
        str = 'ACESSO NEGADO'
        break;
      case 404:
        typeMessage = 'error'
        str = 'ROTA DE APLICAÇÃO NÃO DISPONÍVEL'
        break;
      case 406:
        typeMessage = 'warn'
        str = 'DADOS NÃO VALIDADOS'
        break;
      case 500:
        typeMessage = 'error'
        str = 'ERRO INTERNO DA APLICAÇÃO'
        break;
      default:
        typeMessage = 'error'
        str = 'PROBLEMA AO CARREGAR ' + schema.toLocaleUpperCase();
        break;
    }


    if (schema.localeCompare('usuarios/login') != 0 || res.status == 400) {
      this.MessagesAlertsService.growl(str.trim(), typeMessage);
    }
  }
}