import { TestBed, inject } from '@angular/core/testing';

import { MessagesAlertsService } from './messages-alerts.service';

describe('MessagesAlertsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessagesAlertsService]
    });
  });

  it('should ...', inject([MessagesAlertsService], (service: MessagesAlertsService) => {
    expect(service).toBeTruthy();
  }));
});
