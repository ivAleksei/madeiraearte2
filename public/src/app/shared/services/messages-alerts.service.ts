import { Injectable } from '@angular/core';

import { NotificationsService } from 'angular2-notifications'
import { Observable } from 'rxjs/Observable';
import { SweetAlertService } from 'ng2-sweetalert2';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MessagesAlertsService {
  constructor(
    private NotificationsService: NotificationsService,
    private SweetAlertService: SweetAlertService
  ) { }

  growl(str: string, type: string) {
    this.NotificationsService.create('', str, type, {
      timeOut: 3000,
      pauseOnHover: true,
      clickToClose: true
    });
  }

  deleteConfirmation(): Promise<boolean> {
    return this.SweetAlertService.swal({
      title: 'Você confirma a exclusão dos dados?',
      text: 'Não será possível recuperar posteriormente!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, confirmo!',
      cancelButtonColor: '#3085d6',
      cancelButtonText: 'Não, cancelar.',
      confirmButtonColor: '#d33',
    }).then(function () {
      return new Promise((resolve, reject) => {
        resolve(true);
      });
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        return new Promise((resolve, reject) => {
          resolve(false);
        });
      }
    })
  }

  customSwal(content: any) {
    this.SweetAlertService.swal(content);
  }

  askConfirmation(title: string, text: string) {
    return this.SweetAlertService.swal({
      title: title,
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, confirmo!',
      cancelButtonColor: '#3085d6',
      cancelButtonText: 'Não, cancelar.',
      confirmButtonColor: '#d33',
    }).then(function () {
      return new Promise((resolve, reject) => {
        resolve(true);
      });
    }, function (dismiss) {
      if (dismiss === 'cancel') {
        return new Promise((resolve, reject) => {
          resolve(false);
        });
      }
    })
  }
}
