import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CurrencyMaskModule } from "ng2-currency-mask";

import { ButtonComponent } from './components/button/button.component';
import { ToggleAtivoComponent } from './components/toggle-ativo/toggle-ativo.component';
import { ModPagAcoesComponent } from './components/mod-pag-acoes/mod-pag-acoes.component';
import { SelectBancosComponent } from './components/select-bancos/select-bancos.component';

// WIDGETS COMPONENTS
import { AgendaComponent } from '../widgets/agenda/agenda.component'
import { TodoComponent } from '../widgets/todo/todo.component'


import { FgLineDirective } from './directives/fg-line/fg-line.directive';
import { SubmenuToggleDirective } from './directives/submenu-toggle/submenu-toggle.directive';
import { PerfectScrollbarDirective } from './directives/perfect-scrollbar/perfect-scrollbar.directive';
import { MaskPluginDirective } from './directives/mask-plugin/mask-plugin.directive';
import { DatetimepickerDirective } from './directives/datetimepicker/datetimepicker.directive';
import { BootstrapSelectDirective } from './directives/bootstrap-select/bootstrap-select.directive';
import { ListGroupDirective } from './directives/list-group/list-group.directive';
import { HiTriggerDirective } from './directives/hi-trigger/hi-trigger.directive';
import { CepRequestDirective } from './directives/cep-request/cep-request.directive';
import { InputObrigatorioDirective } from './directives/input-obrigatorio/input-obrigatorio.directive';
import { FullCalendarDirective } from './directives/full-calendar/full-calendar.directive';
import { InputCurrencyMaskDirective } from './directives/input-currency-mask/input-currency-mask.directive';



@NgModule({
  imports: [
    CommonModule,
    CurrencyMaskModule,
    NgxDatatableModule,
    FormsModule,
  ],
  declarations: [
    // COMPONENTS
    ButtonComponent,
    ToggleAtivoComponent,
    ModPagAcoesComponent,
    SelectBancosComponent,
    AgendaComponent,
    TodoComponent,
    // DIRECTIVES
    FgLineDirective,
    SubmenuToggleDirective,
    PerfectScrollbarDirective,
    MaskPluginDirective,
    DatetimepickerDirective,
    BootstrapSelectDirective,
    ListGroupDirective,
    HiTriggerDirective,
    CepRequestDirective,
    InputObrigatorioDirective,
    FullCalendarDirective,
    InputCurrencyMaskDirective,
  ],
  exports: [
    // MODULES
    NgxDatatableModule,
    CurrencyMaskModule,
    FormsModule,
    // COMPONENTS
    ButtonComponent,
    ToggleAtivoComponent,
    ModPagAcoesComponent,
    SelectBancosComponent,
    AgendaComponent,
    TodoComponent,
    // DIRECTIVES
    FgLineDirective,
    SubmenuToggleDirective,
    PerfectScrollbarDirective,
    MaskPluginDirective,
    DatetimepickerDirective,
    BootstrapSelectDirective,
    ListGroupDirective,
    HiTriggerDirective,
    CepRequestDirective,
    InputObrigatorioDirective,
    InputCurrencyMaskDirective,
  ]
})
export class SharedModule { }
