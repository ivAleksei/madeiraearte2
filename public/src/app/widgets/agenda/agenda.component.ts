import { Component, OnInit, Input } from '@angular/core';
import { MessagesAlertsService } from '../../shared/services/messages-alerts.service'
import { ApiService } from '../../shared/services/api.service'

declare var $: any
declare var moment: any

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {

  hoje: any = new Date()

  @Input() defaultView: any
  @Input() agenda_id: any
  @Input() eventos: any

  constructor(
    private MessagesAlertsService: MessagesAlertsService,
    private ApiService: ApiService,
  ) { }

  ngOnInit() {

  }

  add() {
    let target = $('[FullCalendar]')

    var eventTitle = $('#new-event-title').val();

    if (eventTitle != '') {
      let obj: any = {
        title: eventTitle,
        start: moment($('#new-event-start').val(), "DD/MM/YYYY HH:mm").format('YYYY-MM-DD HH:mm'),
        end: moment($('#new-event-end').val(), "DD/MM/YYYY HH:mm").format('YYYY-MM-DD HH:mm'),
        allDay: $('#new-all-day-check').is(":checked"),
        className: $('.event-tag input:checked').val()
      }

      obj.ativo = true
      this.ApiService.post('agenda', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))
            obj = this.parseForm(obj)

            target.fullCalendar('renderEvent', obj, true);

          }
        })

      $('.form-event')[0].reset()
      $('#modal-new-event').modal('hide');
      $('#new-event-title').closest('.form-group').removeClass('has-error');
    }
    else {
      $('#new-event-title').closest('.form-group').addClass('has-error');
      $('#new-event-title').focus();
    }
  }

  update() {
    let target = $('[FullCalendar]')
    let obj: any = {
      _id: $('.edit-event-id').val(),
      title: $('.edit-event-title').val(),
      description: $('.edit-event-description').val(),
      allDay: true,
      className: $('.event-tag-edit input:checked').val()
    }

    if (obj.title != '') {
      this.ApiService.put('agenda', obj, null).then(data => {
        if (data) {
          let obj = JSON.parse(JSON.stringify(data))
          obj = this.parseForm(obj)
          let currentEvent = target.fullCalendar('clientEvents', obj.id);

          currentEvent[0].title = obj.title
          currentEvent[0].description = obj.description
          currentEvent[0].className = obj.className

          target.fullCalendar('updateEvent', currentEvent[0]);

          $('#modal-edit-event').modal('hide');
        }
      })
    }
    else {
      $('.edit-event-title').closest('.form-group').addClass('has-error');
      $('.edit-event-title').focus();
    }
  }

  remove(eventos: any[]) {

    let target = $('[FullCalendar]')
    var currentId = $('.edit-event-id').val();

    let obj = { _id: currentId }
    this.ApiService.delete('agenda', obj).then(data => {
      if (data) {

        $('#modal-edit-event').modal('hide');

        target.fullCalendar('removeEvents', currentId);

        this.eventos = eventos.filter(function (f: any) {
          if (f._id != obj._id) return f
        })
      }
    })
  }

  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    parse.id = parse._id
    return parse
  }
}
