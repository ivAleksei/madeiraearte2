import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service'

declare var $: any

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  edit: Boolean = false;

  tarefas: any[] = [];

  constructor(
    private ApiService: ApiService
  ) {
    this.loadTarefas()
  }

  ngOnInit() {
  }

  loadTarefas(): void {

    this.ApiService.get('todo')
      .then((data) => {

        this.tarefas = data.map((mod: any) => {
          return this.parseForm(mod)
        })
      })
  }

  add(form_directive: any) {
    var obj = this.stringForm(form_directive)

    form_directive.value.ativo = true
    form_directive.value.finalizada = false

    if (obj)
      this.ApiService.post('todo', obj, null)
        .then(data => {
          if (data) {
            let obj = JSON.parse(JSON.stringify(data))

            obj = this.parseForm(obj)
            this.tarefas.push(obj)
            this.clear(form_directive)

          }
        })
  }

  update(form_directive: any, tarefas: any): void {
    let obj = this.stringForm(form_directive)

    this.ApiService.put('todo', obj, null).then(data => {
      if (data) {
        let obj = JSON.parse(JSON.stringify(data))

        let parse = this.parseForm(obj)
        this.tarefas = tarefas.map(function (mod: any) {
          if (mod._id == form_directive.value._id) {
            mod = parse
          }
          return mod
        })

        this.edit = false
        this.clear(form_directive)
      }
    })
  }

  finaliza(obj: any) {
    obj.finalizada = !obj.finalizada
    let parse = {
      _id: obj._id,
      finalizada: obj.finalizada
    }
    this.ApiService.put('todo', parse, null)
  }

  remove(obj: any, tarefas: any): void {

    this.ApiService.delete('todo', obj).then(data => {
      if (data) {
        this.tarefas = tarefas.filter(function (f: any) {
          if (f._id != obj._id) return f
        })
      }
    })
  }

  archive(obj: any, tarefas: any): void {
    obj.ativo = false

    this.ApiService.put('todo', obj, null).then(data => {
      if (data) {
        this.tarefas = tarefas.filter(function (f: any) {
          if (f._id != obj._id) return f
        })
      }
    })
  }

  select(object: any, form_directive: any): void {
    this.edit = true

    object = this.parseForm(object)

    $('.t-add').addClass('toggled')

    form_directive.form.setValue(object)
  }

  stringForm(form_directive: any): any {
    return form_directive.value
  }


  parseForm(obj: any): any {
    let parse = JSON.parse(JSON.stringify(obj))
    delete parse.__v
    delete parse.$$index
    delete parse.edit
    return parse
  }


  clear(form_directive: any): void {
    this.edit = false
    form_directive.form.reset()
  }

  ngAfterViewInit() {
    $('[data-ma-action="todo-form-open"]').click(function () {
      $('.t-add').addClass('toggled')
    })
    $('[data-ma-action="todo-form-close"]').click(function () {
      $('.t-add').removeClass('toggled')
    })

  }

}
