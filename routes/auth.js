// auth.js
  var express = require('express');
  var moment  = require('moment');
  var jwt     = require('jwt-simple');
  var fs      = require('fs');
  var path    = require('path');
  var config  = require('config');

  var middlewareAuth = function(req, res, next){

    var service_access = req.headers['x-service-access'];
  
    //if(service_access)
      return next()

    var token = req.query.token || req.headers['x-access-token'];       

    if(!token){
      var err = new Error('Forbidden');
      err.status = 403;
      return next(err);
    }
    try {
      var decoded = jwt.decode(token, config.get('jwtTokenSecret'));
      var isExpired = moment(decoded.exp).isBefore(new Date());
      
      if(isExpired){
        var err = new Error('Unauthorized');
        err.status = 401;
        return next(err);
      }else{
        req.user = decoded.user;
        next();
      }
    } catch(err){
      return next(err);

    }
  };

  module.exports = middlewareAuth;
