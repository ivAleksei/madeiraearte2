// Ambientes.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var AmbienteSchema = {
  nome: String,
  ativo: Boolean
};

var AmbienteModel = require('../../models/GenericModel')(mongoose, 'Ambiente', AmbienteSchema, '');
var AmbienteController = require('../../controllers/GenericController')(AmbienteModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, AmbienteController.getActive.bind(AmbienteController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, AmbienteController.getAll.bind(AmbienteController));

// GET UM PELO ID
router.get('/:_id', auth, AmbienteController.getById.bind(AmbienteController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, AmbienteController.create.bind(AmbienteController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, AmbienteController.update.bind(AmbienteController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, AmbienteController.remove.bind(AmbienteController));



module.exports = router;