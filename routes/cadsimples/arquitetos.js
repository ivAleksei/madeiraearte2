// Arquitetos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ArquitetoSchema = {
  nome: String,
  cpf: String,
  dataNascimento: String,
  contato: {
    fixo: String,
    celular: String,
    outrosTelefones: [],
    email: String,
    website: String,
    fax: String
  },
  endereco: {
    cep: String,
    logradouro: String,
    numero: String,
    bairro: String,
    cidade: String,
    localidade: String,
    uf: String,
    complemento: String
  },
  dadosbancarios:{
    banco: String,
    agencia: String,
    conta: String,
    observacoesconta: String
  },
  observacoes: String,
  ativo: Boolean
};

var ArquitetoModel = require('../../models/GenericModel')(mongoose, 'Arquiteto', ArquitetoSchema, '');
var ArquitetoController = require('../../controllers/GenericController')(ArquitetoModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ArquitetoController.getActive.bind(ArquitetoController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ArquitetoController.getAll.bind(ArquitetoController));

// GET UM PELO ID
router.get('/:_id', auth, ArquitetoController.getById.bind(ArquitetoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ArquitetoController.create.bind(ArquitetoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ArquitetoController.update.bind(ArquitetoController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ArquitetoController.remove.bind(ArquitetoController));



module.exports = router;