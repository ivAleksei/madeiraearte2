// Clientes.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ClienteSchema = {
  nome: String,
  tipoPessoa: String,
  nomeFantasia: String,
  documentacao: {
    insc_estadual: String,
    cpf: String,
    rg: String
  },
  contato: {
    principal: {
      nome: String,
      fixo: String,
      celular: String,
      data_nascimento: Date
    },
    secundario: {
      nome: String,
      fixo: String,
      celular: String,
      data_nascimento: Date
    },
    email: String,
    website: String,
    fax: String
  },
  endereco: {
    cep: String,
    logradouro: String,
    numero: String,
    bairro: String,
    localidade: String,
    uf: String,
    complemento: String
  },
  bancarios: {
    cod_febraban: Number,
    agencia: String,
    conta_corrente: String
  },
  observacoes: String,
  ativo: Boolean
};

var ClienteModel = require('../../models/GenericModel')(mongoose, 'Cliente', ClienteSchema, '');
var ClienteController = require('../../controllers/GenericController')(ClienteModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ClienteController.getActive.bind(ClienteController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ClienteController.getAll.bind(ClienteController));

// GET UM PELO ID
router.get('/:_id', auth, ClienteController.getById.bind(ClienteController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ClienteController.create.bind(ClienteController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ClienteController.update.bind(ClienteController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ClienteController.remove.bind(ClienteController));



module.exports = router;