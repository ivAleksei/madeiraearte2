// Fornecedores.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var FornecedorSchema = {
  nome: String,
  nomeFantasia: String,
  dataFundacao: Date,
  documentacao: {
    insc_estadual: String,
    cnpj: String
  },
  contato: {
    principal: {
      nome: String,
      fixo: String,
      celular: String,
      data_nascimento: Date
    },
    email: String,
    website: String,
    fax: String
  },
  endereco: {
    cep: String,
    logradouro: String,
    numero: String,
    bairro: String,
    localidade: String,
    uf: String,
    complemento: String
  },
  bancarios: {
    cod_febraban: Number,
    agencia: String,
    conta_corrente: String
  },
  observacoes: String,
  ativo: Boolean
};

var FornecedorModel = require('../../models/GenericModel')(mongoose, 'Fornecedor', FornecedorSchema, '');
var FornecedorController = require('../../controllers/GenericController')(FornecedorModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, FornecedorController.getActive.bind(FornecedorController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, FornecedorController.getAll.bind(FornecedorController));

// GET UM PELO ID
router.get('/:_id', auth, FornecedorController.getById.bind(FornecedorController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, FornecedorController.create.bind(FornecedorController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, FornecedorController.update.bind(FornecedorController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, FornecedorController.remove.bind(FornecedorController));



module.exports = router;