// Funcionarios.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var FuncionarioSchema = {
  nome: String,
  cpf: String,
  data_nascimento: Date,
  contato: {
    fixo: String,
    celular: String,
    email: String
  },
  endereco: {
    cep: String,
    logradouro: String,
    numero: String,
    bairro: String,
    localidade: String,
    uf: String,
    complemento: String
  },
  observacoes: String,
  ativo: Boolean
};

var FuncionarioModel = require('../../models/GenericModel')(mongoose, 'Funcionario', FuncionarioSchema, '');
var FuncionarioController = require('../../controllers/GenericController')(FuncionarioModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, FuncionarioController.getActive.bind(FuncionarioController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, FuncionarioController.getAll.bind(FuncionarioController));

// GET UM PELO ID
router.get('/:_id', auth, FuncionarioController.getById.bind(FuncionarioController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, FuncionarioController.create.bind(FuncionarioController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, FuncionarioController.update.bind(FuncionarioController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, FuncionarioController.remove.bind(FuncionarioController));



module.exports = router;