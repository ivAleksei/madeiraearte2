// Materiais.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var MateriaiSchema = {
  descricao: {
    resumida: String,
    detalhada: String
  },
  ativo: Boolean
};

var MateriaiModel = require('../../models/GenericModel')(mongoose, 'Materiais', MateriaiSchema, '');
var MateriaiController = require('../../controllers/GenericController')(MateriaiModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, MateriaiController.getActive.bind(MateriaiController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, MateriaiController.getAll.bind(MateriaiController));

// GET UM PELO ID
router.get('/:_id', auth, MateriaiController.getById.bind(MateriaiController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, MateriaiController.create.bind(MateriaiController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, MateriaiController.update.bind(MateriaiController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, MateriaiController.remove.bind(MateriaiController));



module.exports = router;