// Modelos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ModeloSchema = {
  descricao: {
    resumida: String,
    detalhada: String,
    observacoes: String
  },
  ativo: Boolean
};

var ModeloModel = require('../../models/GenericModel')(mongoose, 'Modelos', ModeloSchema, '');
var ModeloController = require('../../controllers/GenericController')(ModeloModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ModeloController.getActive.bind(ModeloController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ModeloController.getAll.bind(ModeloController));

// GET UM PELO ID
router.get('/:_id', auth, ModeloController.getById.bind(ModeloController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ModeloController.create.bind(ModeloController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ModeloController.update.bind(ModeloController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ModeloController.remove.bind(ModeloController));



module.exports = router;