// Contas.js
var express = require('express');
var router = express.Router();
var request = require('request');
var auth = require('../auth');
var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;
var ContaSchema = {
  nome: String,
  cod_febraban: String,
  agencia: String,
  conta_corrente: String,
  ativo: Boolean
};
var ContaModel = require('../../models/GenericModel')(mongoose, 'Conta', ContaSchema, '');
var ContaController = require('../../controllers/GenericController')(ContaModel);

var ReceitasModel = require('./receitas').model
var DespesasModel = require('./despesas').model

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ContaController.getActive.bind(ContaController));

router.get('/saldo/', auth, function (req, res, next) {
  var saldoContas = []

  req.query = [{
    "$match": {
      "ativo": true,
      "pago": true
    }
  }
    , {
    "$group": {
      "_id": "$pagamento._conta",
      "saldo": { "$sum": "$pagamento.valores.parcelado" }
    }
  }]
  ContaModel.find({ ativo: true }, function (err, contas) {
    ReceitasModel.aggregate(req.query, function (err, dataReceitas) {
      DespesasModel.aggregate(req.query, function (err, dataDespesas) {
        saldoContas = contas.filter(function (c) {
          return dataReceitas.some(function (r) {
            return r._id.toString() == c._id.toString()
          })
        }).map(function (m) {
          return {
            _id: m._id,
            nome: m.nome,
            saldo: (dataReceitas.filter(function (r) {
              return r._id.toString() == m._id.toString()
            }).map(function (r) {
              return r.saldo
            })[0] || 0) - (dataDespesas.filter(function (r) {
              return r._id.toString() == m._id.toString()
            }).map(function (r) {
              return r.saldo
            })[0] || 0)
          }
        })
        res.json(saldoContas)
      })
    })
  })
});

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ContaController.getAll.bind(ContaController));
// GET UM PELO ID
router.get('/:_id', auth, ContaController.getById.bind(ContaController));
// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ContaController.create.bind(ContaController));
// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ContaController.update.bind(ContaController));
// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ContaController.remove.bind(ContaController));

module.exports = router;
