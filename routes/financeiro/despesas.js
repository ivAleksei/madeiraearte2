// Despesas.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var DespesaSchema = {
  ativo: Boolean,
  pago: Boolean,
  _cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
  observacoes: String,
  pagamento: {
    _planocontas: { type: Schema.Types.ObjectId, ref: 'PlanoConta' },
    _conta: { type: Schema.Types.ObjectId, ref: 'Conta' },
    _formapagamentos: { type: Schema.Types.ObjectId, ref: 'FormaPagamento' },
    valores: {
      total: Number,
      parcelado: Number,
    },
    dataReferencia: Date,
    vencimento: Date,
    numeroParcela: Number,
  },
};

var DespesaModel = require('../../models/GenericModel')(mongoose, 'Despesa', DespesaSchema, ' _cliente pagamento._planocontas pagamento._conta pagamento._formapagamentos');
var DespesaController = require('../../controllers/MovimentoFinanceiroController')(DespesaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, DespesaController.getActive.bind(DespesaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', function (req, res, next) {

  var q = req.query;

  var inicio = new Date(q.ano + '-' + q.mes + '-' + 1);
  var fim = new Date(q.ano + '-' + q.mes + '-' + 31);

  if (q.mes && q.ano) {
    req.query = { 'pagamento.vencimento': { $gte: inicio, $lte: fim } }
    next();
  }


}, auth, DespesaController.getAll.bind(DespesaController));

// GET ANOS DE RECEITAS CADASTRADAS
// É necessário definir a query antes
router.get('/reports',
  function (req, res, next) {

    req.query = {}

    next();
  }
  , auth, DespesaController.getAll.bind(DespesaController));

// ACUMULA SALDO CONTA
router.get('/arc', auth, DespesaController.getAnoMovimentos.bind(DespesaController));

// ACUMULA SALDO CONTA
router.get('/balanco', auth, DespesaController.getBalanco.bind(DespesaController));

// GET UM PELO ID
router.get('/:_id', auth, DespesaController.getById.bind(DespesaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, DespesaController.create.bind(DespesaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, DespesaController.update.bind(DespesaController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, DespesaController.removeArray.bind(DespesaController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, DespesaController.remove.bind(DespesaController));



module.exports = {
  router: router,
  model: DespesaModel
};