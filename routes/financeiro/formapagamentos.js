// FormaPagamentos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var FormaPagamentoSchema = {
  nome: String,
  observacoes: String,
  parcelamento: {
    tipo: String,
    numero: Number
  },
  ativo: Boolean
};

var FormaPagamentoModel = require('../../models/GenericModel')(mongoose, 'FormaPagamento', FormaPagamentoSchema, '');
var FormaPagamentoController = require('../../controllers/GenericController')(FormaPagamentoModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, FormaPagamentoController.getActive.bind(FormaPagamentoController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, FormaPagamentoController.getAll.bind(FormaPagamentoController));

// GET UM PELO ID
router.get('/:_id', auth, FormaPagamentoController.getById.bind(FormaPagamentoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, FormaPagamentoController.create.bind(FormaPagamentoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, FormaPagamentoController.update.bind(FormaPagamentoController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, FormaPagamentoController.remove.bind(FormaPagamentoController));



module.exports = router;