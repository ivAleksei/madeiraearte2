// PlanoContas.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var PlanoContaSchema = {
  nome: String,
  ativo: Boolean
};

var PlanoContaModel = require('../../models/GenericModel')(mongoose, 'PlanoConta', PlanoContaSchema, '');
var PlanoContaController = require('../../controllers/GenericController')(PlanoContaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, PlanoContaController.getActive.bind(PlanoContaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, PlanoContaController.getAll.bind(PlanoContaController));

// GET UM PELO ID
router.get('/:_id', auth, PlanoContaController.getById.bind(PlanoContaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, PlanoContaController.create.bind(PlanoContaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, PlanoContaController.update.bind(PlanoContaController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, PlanoContaController.remove.bind(PlanoContaController));



module.exports = router;