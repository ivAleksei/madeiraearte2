// Receitas.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ReceitaSchema = {
  ativo: Boolean,
  pago: Boolean,
  _cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
  observacoes: String,
  pagamento: {
    _planocontas: { type: Schema.Types.ObjectId, ref: 'PlanoConta' },
    _conta: { type: Schema.Types.ObjectId, ref: 'Conta' },
    _formapagamentos: { type: Schema.Types.ObjectId, ref: 'FormaPagamento' },
    valores: {
      total: Number,
      parcelado: Number,
    },
    dataReferencia: Date,
    vencimento: Date,
    numeroParcela: Number,
  },
};

var ReceitaModel = require('../../models/GenericModel')(mongoose, 'Receita', ReceitaSchema, ' _cliente pagamento._planocontas pagamento._conta pagamento._formapagamentos');
var ReceitaController = require('../../controllers/MovimentoFinanceiroController')(ReceitaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ReceitaController.getActive.bind(ReceitaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', function (req, res, next) {

  var q = req.query;

  var inicio = new Date(q.ano + '-' + q.mes + '-' + 1);
  var fim = new Date(q.ano + '-' + q.mes + '-' + 31);

  if (q.mes && q.ano) {
    req.query = { 'pagamento.vencimento': { $gte: inicio, $lte: fim } }
    next();
  }

}, auth, ReceitaController.getAll.bind(ReceitaController));

// GET ANOS DE RECEITAS CADASTRADAS
// É necessário definir a query antes
router.get('/reports',
  function (req, res, next) {

    req.query = {}

    next();
  }
  , auth, ReceitaController.getAll.bind(ReceitaController));

// ACUMULA SALDO CONTA
router.get('/arc', auth, ReceitaController.getAnoMovimentos.bind(ReceitaController));

// ACUMULA SALDO CONTA
router.get('/balanco', auth, ReceitaController.getBalanco.bind(ReceitaController));

// GET UM PELO ID
router.get('/:_id', auth, ReceitaController.getById.bind(ReceitaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ReceitaController.create.bind(ReceitaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ReceitaController.update.bind(ReceitaController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, ReceitaController.removeArray.bind(ReceitaController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, ReceitaController.remove.bind(ReceitaController));



module.exports = {
  router: router,
  model: ReceitaModel
};