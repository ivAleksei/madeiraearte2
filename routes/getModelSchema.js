// getModelSchema.js
var express = require('express');
var fs = require('fs');
var path = require('path');

var middlewareAuth = function (req, res, next) {
  var schemaFileModel = req.params.schema;
  fs.readFile(path.join(__dirname, '../models/imports/' + schemaFileModel + '.json'), 'utf8', function (err, data) {
    if (err) {
      err.status = 404;
      err.message = 'Modelo de importação não encontrado'
      return next(err);
    }
    var parse = JSON.parse(data)
    var schema = {};
    var populate = function (obj, c) {
      for (let f in obj)
        if (String(obj[f])) {
          if (typeof obj[f] == 'object' && !obj['data_type']) {
            populate(obj[f], (c ? c + '.' + f : f))
          } else {
            var target = schema;
            var model = obj;
            var split = c.split('.');
            for (var s in split) {
              if (!target[split[s]])
                target[split[s]] = {}

              if (s != split.length - 1) {
                target = target[split[s]]
              } else {
                //target[split[s]] = ""
                switch (model['data_type']) {
                  case 'String':
                    target[split[s]] = String;
                    break;
                  case 'Date':
                    target[split[s]] = Date;
                    break;
                  case 'Number':
                    target[split[s]] = Number;
                    break;
                  case 'Boolean':
                    target[split[s]] = Boolean;
                    break;
                }
              }
            }

          }
        }
    }
    populate(parse, '')
    req.ModelSchema = schema;
    next();
  })
};

module.exports = middlewareAuth;

