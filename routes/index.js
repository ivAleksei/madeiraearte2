var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
  res.redirect('/ws')
})

router.use('/ws', require('./ws'));

module.exports = router;

