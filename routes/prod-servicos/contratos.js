// Contratos.js

var express = require('express');
var router = express.Router();
var numeral = require('numeral');
var moment = require('moment');
var ptBr = require('numeral/locales/pt-br');
numeral.locale('pt-br');


var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var OrcamentoModel = require('./orcamentos').model
var OrcamentoItensModel = require('./orcamentos_itens').model
var OrcamentoMoveisModel = require('./orcamentos_moveis').model

var ContratoSchema = {
  _orcamento: { type: Schema.Types.ObjectId, ref: 'Orcamento' },
  tipoContrato: String,
  ativo: Boolean
};



var ContratoModel = require('../../models/GenericModel')(mongoose, 'Contrato', ContratoSchema, '_orcamento _orcamento._cliente _orcamento._arquiteto');
var ContratoController = require('../../controllers/GenericController')(ContratoModel);

// ROTAS
function moeda(valor, casas, separdor_decimal, separador_milhar) {

  var valor_total = parseInt(valor * (Math.pow(10, casas)));
  var inteiros = parseInt(parseInt(valor * (Math.pow(10, casas))) / parseFloat(Math.pow(10, casas)));
  var centavos = parseInt(parseInt(valor * (Math.pow(10, casas))) % parseFloat(Math.pow(10, casas)));


  if (centavos % 10 == 0 && centavos + "".length < 2) {
    centavos = centavos + "0";
  } else if (centavos == 0) {
    var inteiros = inteiros % 1000;
    if (inteiros == 0) {
      inteiros = "000";
    } else if (inteiros < 10) {
      inteiros = "00" + inteiros;
    } else if (inteiros < 100) {
      inteiros = "0" + inteiros;
    }
    var retorno = milhar(milhares, separador_milhar) + "" + separador_milhar + "" + inteiros;
    return retorno;
  }
  else {
    return inteiros;
  }

}

// REPORT
router.get('/reports', auth, ContratoController.getActive.bind(ContratoController));

// GET ATIVOS SE EXISTIR A FLAG
router.get('/', ContratoController.getActive.bind(ContratoController));

// GET CONTRATOS DETALHADAMENTE
router.get('/detalhado', function (req, res, next) {
  req.query = { ativo: true }
  ContratoModel.find(req.query, function (err, dataContratos) {
    var queryOrcamento = dataContratos.map(c => {
      return {
        _id: c._orcamento._id
      }
    })
    OrcamentoModel.find(queryOrcamento, function (err, dataOrcamentos) {
      if (err) return next(err);
      OrcamentoItensModel.find(queryOrcamento, function (err, dataItens) {
        if (err) return next(err);
        OrcamentoMoveisModel.find(queryOrcamento, function (err, dataMoveis) {
          var result =
            dataOrcamentos.map(o => {
              return {
                _id: o._id,
                _cliente: o._cliente,
                _arquiteto: o._arquiteto,
                observacoes: o.observacoes,
                _formapagamento: o._formapagamento,
                aprovado: o.aprovado,
                ativo: o.ativo,
                valorTotal: numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
                identificacao: 'Cliente: ' + o._cliente.nome + ' - Arquiteto(a): ' + o._arquiteto.nome + '- ' + numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
                moveis: dataMoveis.map(m => {
                  return {
                    descricao: m.descricao,
                    _ambiente: m._ambiente,
                    _orcamento: m._orcamento,
                    valorMovel: numeral(dataItens.filter(i => String(i._movel) == String(m._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
                    itens: dataItens.filter(i => {
                      return (String(i._movel) == String(m._id))
                    }).map(i => {
                      return {
                        _item: i._item,
                        ativo: i.ativo,
                        quantidade: i.quantidade,
                        medidas: i.medidas,
                        valores: i.valores,
                        observacoes: i.observacoes,
                        fake_id: i.fake_id
                      }
                    })
                  }
                })
              }
            });
          res.json(result);
        })
      })
    })
  })
});


// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ContratoController.getAll.bind(ContratoController));

// GET UM PELO ID
router.get('/:_id', auth, ContratoController.getById.bind(ContratoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ContratoController.create.bind(ContratoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ContratoController.update.bind(ContratoController));

router.get('/reports/:_id', function (req, res, next) {
  req.query = { _id: req.params._id }
  var queryComp = { _orcamento: req.params._id }

  OrcamentoModel.find(req.query, function (err, dataOrcamentos) {
    OrcamentoItensModel.find(queryComp, function (err, dataItens) {
      if (err) return next(err);
      OrcamentoMoveisModel.find(queryComp, function (err, dataMoveis) {
        var result =
          dataOrcamentos.map(o => {
            return {
              _id: o._id,
              _cliente: o._cliente,
              _arquiteto: o._arquiteto,
               _formapagamento: o._formapagamento,
              formapagamento:  o._formapagamento.parcelamento ? "parcelado em " + o._formapagamento.parcelamento.numero + "x " : "a vista",
              observacoes: o.observacoes,
              aprovado: o.aprovado,
              entrega: { 
                local: o.entrega.local, 
                datas: {
                       definitiva: moment(o.entrega.datas.definitiva).format('DD/MM/YYYY'),
                       prevista: moment(o.entrega.datas.prevista).format('DD/MM/YYYY')
              }},
              ativo: o.ativo,
              valorTotal: numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
              identificacao: 'Cliente: ' + o._cliente.nome + ' - Arquiteto(a): ' + o._arquiteto.nome + '- ' + numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
              moveis: dataMoveis.map(m => {
                return {
                  descricao: m.descricao,
                  _ambiente: m._ambiente,
                  _orcamento: m._orcamento,
                  valorMovel: numeral(dataItens.filter(i => String(i._movel) == String(m._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
                  itens: dataItens.filter(i => {
                    return (String(i._movel) == String(m._id))
                  }).map(i => {
                    return {
                      _item: i._item,
                      ativo: i.ativo,
                      quantidade: i.quantidade,
                      medidas: i.medidas,
                      valores: i.valores,
                      observacoes: i.observacoes,
                      fake_id: i.fake_id
                    }
                  })
                }
              })
            }
          });
        res.json(result);
      })
    })
  })
})



// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ContratoController.remove.bind(ContratoController));




module.exports = router;