// Esquadrias.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var EsquadriaSchema = {
  observacoes: String,
  ativo: Boolean
};

var EsquadriaModel = require('../../models/GenericModel')(mongoose, 'Esquadria', EsquadriaSchema, '');
var EsquadriaController = require('../../controllers/GenericController')(EsquadriaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, EsquadriaController.getActive.bind(EsquadriaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, EsquadriaController.getAll.bind(EsquadriaController));

// GET ANOS DE EsquadriaS CADASTRADAS
// É necessário definir a query antes
router.get('/reports', function (req, res, next) {

  req.query = {}

  next();
}
  , auth, EsquadriaController.getAll.bind(EsquadriaController));

// GET UM PELO ID
router.get('/:_id', auth, EsquadriaController.getById.bind(EsquadriaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, EsquadriaController.create.bind(EsquadriaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, EsquadriaController.update.bind(EsquadriaController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, EsquadriaController.removeArray.bind(EsquadriaController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, EsquadriaController.remove.bind(EsquadriaController));



module.exports = router;