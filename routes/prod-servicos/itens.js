// Items.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ItemSchema = {
  nome: String,
  valor: Number,
  ativo: Boolean,
  medicao: Number,
  observacoes: String  
};

var ItemModel = require('../../models/GenericModel')(mongoose, 'Item', ItemSchema, '_cliente _arquiteto moveis._ambiente');
var ItemController = require('../../controllers/GenericController')(ItemModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ItemController.getActive.bind(ItemController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ItemController.getAll.bind(ItemController));

// GET ANOS DE ItemS CADASTRADAS
// É necessário definir a query antes
router.get('/reports', function (req, res, next) {

  req.query = {}

  next();
}
  , auth, ItemController.getAll.bind(ItemController));

// GET UM PELO ID
router.get('/:_id', auth, ItemController.getById.bind(ItemController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ItemController.create.bind(ItemController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ItemController.update.bind(ItemController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, ItemController.removeArray.bind(ItemController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, ItemController.remove.bind(ItemController));



module.exports = router;