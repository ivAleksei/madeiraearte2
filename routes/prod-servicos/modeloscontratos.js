// ModelosContratos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var ModeloContratoSchema = {
  nome: String,
  textomodelo: String,
  ativo: Boolean
};

var ModeloContratoModel = require('../../models/GenericModel')(mongoose, 'ModeloContrato', ModeloContratoSchema, '');
var ModeloContratoController = require('../../controllers/GenericController')(ModeloContratoModel);

// ROTAS

// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ModeloContratoController.getActive.bind(ModeloContratoController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ModeloContratoController.getAll.bind(ModeloContratoController));

// REPORT
router.get('/reports', auth, ModeloContratoController.getActive.bind(ModeloContratoController));

// GET UM PELO ID
router.get('/:_id', auth, ModeloContratoController.getById.bind(ModeloContratoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ModeloContratoController.create.bind(ModeloContratoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ModeloContratoController.update.bind(ModeloContratoController));



// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ModeloContratoController.remove.bind(ModeloContratoController));



module.exports = router;