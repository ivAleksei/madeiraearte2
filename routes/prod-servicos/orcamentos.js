// Orcamentos.js

var express = require('express');
var router = express.Router();
var numeral = require('numeral');
var ptBr = require('numeral/locales/pt-br');
numeral.locale('pt-br');

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var OrcamentoItensModel = require('./orcamentos_itens').model
var OrcamentoMoveisModel = require('./orcamentos_moveis').model

var OrcamentoSchema = {
  _cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
  _arquiteto: { type: Schema.Types.ObjectId, ref: 'Arquiteto' },
  _formapagamento: { type: Schema.Types.ObjectId, ref: 'FormaPagamento' },
  entrega: {
    local: {
      cep: String,
      logradouro: String,
      numero: String,
      bairro: String,
      localidade: String,
      uf: String,
      complemento: String
    },
    datas: {
      prevista: Date,
      definitiva: Date
    }
  },
  observacoes: {
    itens: {
      inclusos: String,
      nao_inclusos: String
    },
    moveis: String,
    esquadrias: String
  },
  aprovado: Boolean,
  ativo: Boolean
};

var OrcamentoModel = require('../../models/GenericModel')(mongoose, 'Orcamento', OrcamentoSchema, '_cliente _arquiteto _formapagamento');
var OrcamentoController = require('../../controllers/GenericController')(OrcamentoModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', OrcamentoController.getActive.bind(OrcamentoController));

router.get('/aprovados', function (req, res, next) {
  req.query = { aprovado: true }
  OrcamentoModel.find(req.query, function (err, dataOrcamentos) {
    var querycomp = dataOrcamentos.map(o => {
      return {
        _orcamento: o._id
      }
    });
    OrcamentoItensModel.find(querycomp, function (err, dataItens) {
      if (err) return next(err);
      OrcamentoMoveisModel.find(querycomp, function (err, dataMoveis) {
        var result =
          dataOrcamentos.map(o => {
            return {
              _id: o._id,
              _cliente: o._cliente,
              _arquiteto: o._arquiteto,
              observacoes: o.observacoes,
              aprovado: o.aprovado,
              ativo: o.ativo,
              valorTotal: numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
              identificacao: 'Cliente: '+ o._cliente.nome + ' - Arquiteto(a): ' +o._arquiteto.nome + '- '+  numeral(dataItens.filter(i => String(i._orcamento) == String(o._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
              moveis: dataMoveis.map(m => {
                return {
                  descricao: m.descricao,
                  _ambiente: m._ambiente,
                  _orcamento: m._orcamento,
                  valorMovel: numeral(dataItens.filter(i => String(i._movel) == String(m._id)).reduce((pre, v) => pre + v.valores.totais.item, 0)).format('$ 0,0.00'),
                  itens: dataItens.filter(i => {
                    return (String(i._movel) == String(m._id))
                  }).map(i => {
                    return {
                      _item: i._item,
                      ativo: i.ativo,
                      quantidade: i.quantidade,
                      medidas: i.medidas,
                      valores: i.valores,
                      observacoes: i.observacoes,
                      fake_id: i.fake_id
                    }
                  })
                }
              })
            }
          });
        res.json(result);
      })
    })
  })
});

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, OrcamentoController.getAll.bind(OrcamentoController));


// GET UM PELO ID
router.get('/:_id', auth, OrcamentoController.getById.bind(OrcamentoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, OrcamentoController.create.bind(OrcamentoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, OrcamentoController.update.bind(OrcamentoController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, OrcamentoController.removeArray.bind(OrcamentoController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, OrcamentoController.remove.bind(OrcamentoController));


module.exports = {
  router: router,
  model: OrcamentoModel
};