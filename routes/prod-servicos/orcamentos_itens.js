// Orcamento_Itens.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var Orcamento_ItenSchema = {
  _item: { type: Schema.Types.ObjectId, ref: 'Item' },
  _movel: { type: Schema.Types.ObjectId, ref: 'Orcamento_Movei' },
  _orcamento: { type: Schema.Types.ObjectId, ref: 'Orcamento' },
  ativo: Boolean,
  quantidade: Number,
  medidas: {
    altura: Number,
    largura: Number,
    profundidade: Number
  },
  valores: {
    acrescimo: Number,
    unitario: Number,
    totais: {
      unitario: Number,
      item: Number
    }
  },
  observacoes: String,
  fake_id: String
}

var Orcamento_ItenModel = require('../../models/GenericModel')(mongoose, 'Orcamento_Iten', Orcamento_ItenSchema, '_item');
var Orcamento_ItenController = require('../../controllers/GenericController')(Orcamento_ItenModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, Orcamento_ItenController.getActive.bind(Orcamento_ItenController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, Orcamento_ItenController.getAll.bind(Orcamento_ItenController));

// GET ANOS DE Orcamento_ItenS CADASTRADAS
// É necessário definir a query antes
router.get('/reports', function (req, res, next) {

  req.query = {}

  next();
}
  , auth, Orcamento_ItenController.getAll.bind(Orcamento_ItenController));

// GET UM PELO ID
router.get('/:_id', auth, Orcamento_ItenController.getById.bind(Orcamento_ItenController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, Orcamento_ItenController.create.bind(Orcamento_ItenController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, Orcamento_ItenController.update.bind(Orcamento_ItenController));

// DELETE - EXLUIR POR QUERY
router.delete('/q', auth, Orcamento_ItenController.removeQuery.bind(Orcamento_ItenController));

// POST - CRIAÇÃO DE ARRAY
router.post('/ar', auth, Orcamento_ItenController.createArray.bind(Orcamento_ItenController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, Orcamento_ItenController.removeArray.bind(Orcamento_ItenController));

// DELETE - EXLUIR POR QUERY
router.delete('/', auth, Orcamento_ItenController.removeQuery.bind(Orcamento_ItenController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, Orcamento_ItenController.remove.bind(Orcamento_ItenController));




module.exports = {
  router: router,
  model: Orcamento_ItenModel
};