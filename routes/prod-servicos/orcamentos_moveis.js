// Orcamento_Moveis.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var Orcamento_MoveiSchema = {
  descricao: String,
  _ambiente: {
    type: Schema.Types.ObjectId,
    ref: 'Ambiente'
  },
  _orcamento: {
    type: Schema.Types.ObjectId,
    ref: 'Orcamento'
  },
  subtotal: Number,
  fake_id: String
}

var Orcamento_MoveiModel = require('../../models/GenericModel')(mongoose, 'Orcamento_Movei', Orcamento_MoveiSchema, '_ambiente');
var Orcamento_MoveiController = require('../../controllers/GenericController')(Orcamento_MoveiModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, Orcamento_MoveiController.getActive.bind(Orcamento_MoveiController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, Orcamento_MoveiController.getAll.bind(Orcamento_MoveiController));

// GET ANOS DE Orcamento_MoveiS CADASTRADAS
// É necessário definir a query antes
router.get('/reports', function (req, res, next) {

  req.query = {}

  next();
}, auth, Orcamento_MoveiController.getAll.bind(Orcamento_MoveiController));

// GET UM PELO ID
router.get('/:_id', auth, Orcamento_MoveiController.getById.bind(Orcamento_MoveiController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, Orcamento_MoveiController.create.bind(Orcamento_MoveiController));

// POST - CRIAÇÃO DE ARRAY
router.post('/ar', auth, Orcamento_MoveiController.createArray.bind(Orcamento_MoveiController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, Orcamento_MoveiController.update.bind(Orcamento_MoveiController));

// DELETE - EXLUIR POR QUERY
router.delete('/q', auth, Orcamento_MoveiController.removeQuery.bind(Orcamento_MoveiController));

// DELETE - EXLUIR ARRAY DE OBJETOS POR ID
router.post('/del', auth, Orcamento_MoveiController.removeArray.bind(Orcamento_MoveiController));

// DELETE - EXLUIR POR UM ID
router.delete('/:_id', auth, Orcamento_MoveiController.remove.bind(Orcamento_MoveiController));



module.exports = {
  router: router,
  model: Orcamento_MoveiModel
};