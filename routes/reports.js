// Reports.js

var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var request = require('request');

var pdf = require('html-pdf');

var auth = require('./auth');

router.get('/', function (req, res, next) {
  var nameRPT = req.query.rpt;
  var schema = req.query.sch;
  var _id = req.query._id;

  // VARIÁVEIS DINÂMICAS
  var printDate = new Date();
  var logoBuffer = fs.readFileSync('public/src/assets/img/logo.png');
  var content = '';

  dataSource = 'http://127.0.0.1:8080/ws/' + schema + '/reports/' + _id;
  dataSource2 = 'http://127.0.0.1:8080/ws/Report/' + nameRPT;

  // DIRETÓRIOS
  var pathHTML = 'reports/common.html';
  var pathPDF = 'reports/tmp/' + nameRPT + printDate.getTime() + '.pdf';

  var html = fs.readFileSync(pathHTML, 'utf8');
  var options = {

    "header": {
      "height": "50mm",
      "contents":
      {
        default:
        `<div class="header">
            <div class="logo-place">
            <img class="logo" width="100" height="67" alt="Embedded Image" src="`
        + 'data:image/png;base64,' + logoBuffer.toString('base64') +
        `" />
            </div>
          </div>`
      }
    },
    "footer": {
      "height": "33mm",
      "contents":
      {
        default:
        `<div class="footer tx-center" style="font-size: 8px; padding-top: 30px;">
            <p class="address">
              Av. Antônio Basílio, 3006 – Sala 411 – Lagoa Center Business – Lagoa Nova CEP: 59065-500 Natal/RN.
            <br>
            <span class="contact">
              Fonefax: 84 3211 4929 / 84 3618 0135 – E-mail: <span class="email">escritoriodeprojetos@supercabo.com.br</span></span>
            </p>
          </div>`
      }

    },
    "border": {
      "top": "0cm",            // default is 0, units: mm, cm, in, px
      "right": "1.5cm",
      "bottom": "0cm",
      "left": "1.5cm"
    },
    "format": 'A4',
    "type": "pdf"
  };

  request(dataSource, function (error, response, body) {
    dataRPT = JSON.parse(body);
    if (dataRPT) {
      // REQUISIÇÃO DE DADOS (JSON) PARA PREENCHIMENTO DO CONTENT
      request.post({
        url: dataSource2,
        json: dataRPT
      }, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          dataRPT.printDate = (printDate.getDate() > 9 ? printDate.getDate() : '0' + printDate.getDate()) + '/' + ((printDate.getMonth() + 1) > 9 ? (printDate.getMonth() + 1) : '0' + (printDate.getMonth() + 1)) + '/' + (1900 + printDate.getYear());
         // console.log(dataRPT);
          content = body;
          // REPLACE PARA DATA DE PRINT
          content = content.replace('dataRPT.printDate', dataRPT.printDate);
          
          // MONTAGEM PDF FINAL
          fs.readFile(pathHTML, 'utf8', function (err, data) {
            var css = fs.readFileSync(path.join(__dirname, '../reports/', 'report.scss'), 'utf8');

            var html = data
              .replace('logo_b64', 'data:image/png;base64,' + logoBuffer.toString('base64'))
              .replace('<div class="content"></div>', content)
              .replace('<style id="css-style"></style>', '<style id="css-style">\n' + css + '\n</style>');

            // res.send(html)
            // return;
            pdf.create(html, options).toFile(pathPDF, function (err, dataPDF) {
              if (err) return console.log(err);
              fs.readFile(pathPDF, function (err, data) {
                res.writeHead(200, { 'Content-Type': 'application/pdf' });
                res.end(data);
                fs.unlinkSync(pathPDF);
              });
            });
          })
        }
      });
    }
  });




  // request(dataSource, function (error, response, body) {
  //   var dataRPT = {
  //     data: JSON.parse(body)
  //     //.filter(function (b) {
  //     //  return b._id == _id;
  //     //})
  //   };


  //   dataRPT.printDate = (printDate.getDate() > 9 ? printDate.getDate() : '0' + printDate.getDate()) + '/' + ((printDate.getMonth() + 1) > 9 ? (printDate.getMonth() + 1) : '0' + (printDate.getMonth() + 1)) + '/' + (1900 + printDate.getYear());

  //   for (b in dataRPT.data) {
  //     content += fs.readFileSync(path.join('reports/' + nameRPT, 'content.html'), 'utf8');
  //     // console.log(dataRPT.data)
  //     // ALIMENTAÇÃO DE CONTENT BASEADO NO JSON RECEBIDO
  //     // FUNÇÃO RECURSIVA - CUIDADO AO MEXER
  //     var populate = function (obj, c) {
  //       for (let f in obj)
  //         if (String(obj[f]))
  //           if (typeof obj[f] == 'object') {
  //             console.log(f);
  //             console.log(Number.isInteger(f) + ' ' + typeof (f))

  //             populate(obj[f], (c ? c + '.' + f : f))
  //           } else {

  //             while (content.indexOf('dataRPT' + (c ? '.' + c : '') + '.' + f) != -1)

  //               content = content.replace('dataRPT' + (c ? '.' + c : '') + '.' + f, obj[f]);
  //           }
  //     }
  //     populate(dataRPT.data[b], '')

  //     // REPLACE PARA DATA DE PRINT
  //     content = content.replace('dataRPT.printDate', dataRPT.printDate);
  //   }
  //   // MONTAGEM PDF FINAL
  //   fs.readFile(pathHTML, 'utf8', function (err, data) {
  //     var css = fs.readFileSync(path.join(__dirname, '../reports/', 'report.scss'), 'utf8');

  //     var html = data
  //       .replace('logo_b64', 'data:image/png;base64,' + logoBuffer.toString('base64'))
  //       .replace('<div class="content"></div>', content)
  //       .replace('<style id="css-style"></style>', '<style id="css-style">\n' + css + '\n</style>');

  //     pdf.create(html, options).toFile(pathPDF, function (err, dataPDF) {
  //       if (err) return console.log(err);
  //       fs.readFile(pathPDF, function (err, data) {
  //         res.writeHead(200, { 'Content-Type': 'application/pdf' });
  //         res.end(data);
  //         fs.unlinkSync(pathPDF);
  //       });
  //     });
  //   })
  // });
});

module.exports = router;