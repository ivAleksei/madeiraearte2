// Modulos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var moduloSchema = {
  nome: String,
  codigo: String,
  icone: String,
  ativo: Boolean
};

var ModuloModel = require('../../models/GenericModel')(mongoose, 'modulo', moduloSchema, '');
var ModuloController = require('../../controllers/GenericController')(ModuloModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, ModuloController.getActive.bind(ModuloController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, ModuloController.getAll.bind(ModuloController));

// REPORT
router.get('/reports', auth, ModuloController.getActive.bind(ModuloController));

// GET UM PELO ID
router.get('/:_id', auth, ModuloController.getById.bind(ModuloController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, ModuloController.create.bind(ModuloController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, ModuloController.update.bind(ModuloController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, ModuloController.remove.bind(ModuloController));



module.exports = router;