// Paginas.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var paginasSchema = {
  nome: String,
  codigo: String,
  _modulo: { type: Schema.Types.ObjectId, ref: 'modulo' },
  ativo: Boolean
};

var PaginaModel = require('../../models/GenericModel')(mongoose, 'pagina', paginasSchema, '_modulo');
var PaginaController = require('../../controllers/GenericController')(PaginaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, PaginaController.getActive.bind(PaginaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, PaginaController.getAll.bind(PaginaController));

// GET UM PELO ID
router.get('/:_id', auth, PaginaController.getById.bind(PaginaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, PaginaController.create.bind(PaginaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, PaginaController.update.bind(PaginaController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, PaginaController.remove.bind(PaginaController));



module.exports = router;