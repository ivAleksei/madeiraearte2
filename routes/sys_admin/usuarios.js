// Usuarios.js
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('config');
var formidable = require('formidable');
var request = require('request');
var md5 = require('md5');
var auth = require('../auth');
var mongoose = require('../../db/mongoose');

var Schema = mongoose.Schema;
var model = {
  login_exib: String,
  login: String,
  nome: String,
  password: String,
  ativo: Boolean,
  user_pic: {
    name: String,
    ext: String
  },
  _modulos: [{ type: Schema.Types.ObjectId, ref: 'modulo' }],
  _paginas: [{ type: Schema.Types.ObjectId, ref: 'pagina' }]
};

var UsuarioModel = require('../../models/GenericModel')(mongoose, 'usuario', model, '_modulos _paginas');
var UsuarioController = require('../../controllers/UsuarioController')(UsuarioModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, UsuarioController.getActive.bind(UsuarioController));
// GET ATIVOS SE EXISTIR A FLAG
router.get('/_info/:_id', auth, UsuarioController.getUserInfo.bind(UsuarioController));
// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, UsuarioController.getAll.bind(UsuarioController));
// GET UM PELO ID
router.get('/:_id', auth, UsuarioController.getById.bind(UsuarioController));
// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, UsuarioController.create.bind(UsuarioController));
// POST - CRIAÇÃO DE OBJETO
router.post('/_pic/:_id', auth, UsuarioController.removePic.bind(UsuarioController));
// POST - CRIAÇÃO DE TOKEN
router.post('/login', function (req, res, next) {
  var obj = req.body;
  UsuarioModel.find({}, function (err, data) {
    if (err) return next(err);
    var usuarios = null;
    usuarios = data;
    let logar = function (obj) {

      /* ENCTRIPTAÇÃO DE DADOS DO USUÁRIO */
      var user = { login: md5(obj.login) }
      user.password = md5(user.login + obj.password)

      var usuarioExiste = false;
      var usuarioPermitido = null;

      usuarios.some(function (b) {
        if (user.login.localeCompare(b.login) == 0)
          usuarioExiste = true;
        if (user.login.localeCompare(b.login) == 0 && user.password.localeCompare(b.password) == 0) {
          usuarioPermitido = b;
        }
      });
      if (usuarioPermitido) {
        var expires = moment().add(1, 'hour').valueOf();
        var token = jwt.encode({
          user: user.login,
          exp: expires
        }, config.get('jwtTokenSecret'));
        request('http://127.0.0.1:8080/ws/usuarios/' + '_info/' + usuarioPermitido._id + '?token=' + token, function (error, response, body) {
          res.json(JSON.parse(body));

        });
      } else {
        var err = new Error('Senha incorreta. Tente novamente.');
        if (!usuarioExiste) {
          var err = new Error('O sistema não reconhece este usuário');
        }
        err.status = 400;
        next(err);
      }
    }
    if (Object.keys(obj).length == 0) {
      var formidable = require('formidable');
      var form = new formidable.IncomingForm();
      form.parse(req, function (err, fields, files) {
        var obj = JSON.parse(fields.data);
        logar(obj);
      });
    } else {
      logar(obj);
    }
  });
});
// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, UsuarioController.update.bind(UsuarioController));
// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, UsuarioController.remove.bind(UsuarioController));

module.exports = router;