// Agendas.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var agendaSchema = {
  title: String,
  start: String,
  end: String,
  allDay: Boolean,
  className: String,
  description: String,
  ativo: Boolean
};

var AgendaModel = require('../../models/GenericModel')(mongoose, 'agenda', agendaSchema, '');
var AgendaController = require('../../controllers/GenericController')(AgendaModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, AgendaController.getActive.bind(AgendaController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, AgendaController.getAll.bind(AgendaController));

// GET UM PELO ID
router.get('/:_id', auth, AgendaController.getById.bind(AgendaController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, AgendaController.create.bind(AgendaController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, AgendaController.update.bind(AgendaController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, AgendaController.remove.bind(AgendaController));



module.exports = router;