// Todos.js

var express = require('express');
var router = express.Router();

var auth = require('../auth');

var mongoose = require('../../db/mongoose');
var Schema = mongoose.Schema;

var TodoSchema = {
  ativo: Boolean,
  finalizada: Boolean,
  descricao: String
};

var TodoModel = require('../../models/GenericModel')(mongoose, 'Todo', TodoSchema, '');
var TodoController = require('../../controllers/GenericController')(TodoModel);

// ROTAS
// GET ATIVOS SE EXISTIR A FLAG
router.get('/', auth, TodoController.getActive.bind(TodoController));

// GET TODOS (INCLUSIVE NÃO ATIVOS)
router.get('/all', auth, TodoController.getAll.bind(TodoController));

// GET UM PELO ID
router.get('/:_id', auth, TodoController.getById.bind(TodoController));

// POST - CRIAÇÃO DE OBJETO
router.post('/', auth, TodoController.create.bind(TodoController));

// PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
router.put('/:_id', auth, TodoController.update.bind(TodoController));

// DELETE - EXLUIR USUARIOS POR UM ID
router.delete('/:_id', auth, TodoController.remove.bind(TodoController));



module.exports = router;