'use strict'

var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');
var auth = require('./auth');
var getModelSchema = require('./getModelSchema');

/* ADMIN DO SISTEMA */
router.use('/usuarios', require('./sys_admin/usuarios'));
router.use('/modulos', require('./sys_admin/modulos'));
router.use('/paginas', require('./sys_admin/paginas'));

/* REPORTS */
router.use('/reports', require('./reports'));

/* WIDGETS */
router.use('/agenda', require('./widgets/agenda'));
router.use('/todo', require('./widgets/todo'));

/* CAD SIMPLES */
router.use('/ambientes', require('./cadsimples/ambientes'));
router.use('/arquitetos', require('./cadsimples/arquitetos'));
router.use('/clientes', require('./cadsimples/clientes'));
router.use('/fornecedores', require('./cadsimples/fornecedores'));
router.use('/funcionarios', require('./cadsimples/funcionarios'));
router.use('/materiais', require('./cadsimples/materiais'));
router.use('/modelos', require('./cadsimples/modelos'));

/* FINANCEIRO */
router.use('/formapagamentos', require('./financeiro/formapagamentos'));
router.use('/planocontas', require('./financeiro/planocontas'));
router.use('/contas', require('./financeiro/contas'));
router.use('/receitas', require('./financeiro/receitas').router);
router.use('/despesas', require('./financeiro/despesas').router);

/* PROD_SERVICOS */
router.use('/orcamentos', require('./prod-servicos/orcamentos').router);
router.use('/orcamentos_moveis', require('./prod-servicos/orcamentos_moveis').router);
router.use('/orcamentos_itens', require('./prod-servicos/orcamentos_itens').router);

router.use('/modeloscontratos', require('./prod-servicos/modeloscontratos'));

router.use('/itens', require('./prod-servicos/itens'));
router.use('/esquadrias', require('./prod-servicos/esquadrias'));
router.use('/contratos', require('./prod-servicos/contratos'));

router.post('/Report/:nameRpt', function (req, res) {
  var nameRpt = req.params.nameRpt;
  var objetos = req.body;
  res.render('Report' + nameRpt, {
    objetos: objetos
  });
})

router.get('/', function (req, res) {
  fs.readFile(path.join(__dirname, '../package.json'), 'utf8', function (err, info) {
    var data = JSON.parse(info);
    res.render('homeAPI', {info : data})
    // res.send(`
    // <h1>`+ data.name + `</h1>
    // <h3>LocalService - Versão `+ data.version + `</h3>
    // `);
  })
});

router.get('/info_ws', function (req, res) {
  fs.readFile(path.join(__dirname, '../package.json'), 'utf8', function (err, info) {

    var data = JSON.parse(info);
    res.send({
      name: data.name,
      version: data.version,
      description: data.description,
      author: data.author
    });
  })
});

router.get('/info_client', function (req, res) {
  fs.readFile(path.join(__dirname, '../public/package.json'), 'utf8', function (err, info) {
    var data = JSON.parse(info);

    res.send({
      name: data.name,
      version: data.version,
      description: data.description,
      author: data.author
    });
  })
});

router.get('/resources', function (req, res, next) {
  var resource = req.query.data;
  var data;

  if (resource) {
    fs.readFile(path.join(__dirname, '../resources/' + resource + '.json'), 'utf8', function (err, data) {
      if (err) {
        err.status = 400;
        err.message = 'Recurso não encontrado'
        return next(err);
      }

      if (data.charCodeAt(0) == 65279)
        data = data.substr(1, data.length);   //  REMOVER CHARACTER BOM 'ï»¿'

      data = JSON.parse(data);
      res.json(data || []);
    });
  }
});

router.get('/files/', function (req, res, next) {
  var type = req.query.type;
  var data = req.query.data;
  var name = req.query.image;
  if (name) {
    var pathImg = path.join(__dirname, '../files/images/' + data + '/' + name);
    fs.stat(pathImg, function (err, stat) {
      if (err)
        return res.json('404 - File not Found');

      fs.readFile(pathImg, function (err, data) {
        if (err) throw err;
        res.writeHead(200, { 'Content-Type': 'image/jpeg' });
        res.end(data);
      });
    });
  } else {
    res.json('No Image');
  }
});

router.post('/import/:schema',getModelSchema, function (req, res, next) {
  res.json(req.ModelSchema)
});

module.exports = router;
